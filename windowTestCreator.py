from tkinter import *

# First create the Window class which inhearets from Frame, a tkinter class

class Window(Frame):

    # Define settings upon initialization, here you can specify
    def __init__(self, master=None):

        # Paramaters that I want to send through the Frame class
        Frame.__init__(self, master)

        # Refrence to the master widget, which is the tk window
        self.master = master

        # Next we want to run init_window
        self.init_window()

    # Creation of init_window
    def init_window(self):

        # Changing the title of the master widget
        self.master.title("GUI TEST")

        # allowing the widget to take up the full space of the root window
        self.pack(fill=BOTH, expand=1)

        # Creating a menu instance
        menu = Menu(self.master)
        self.master.config(menu=menu)

        # Create the file object
        file = Menu(menu)

        # adds a command to the menu option, callit it exit and the command it runs on event is clientExit
        file.add_command(label="Exit", command=self.clientExit)

        # added "file" to our menu
        menu.add_cascade(label="File", menu=file)

        # create the edit object
        edit = Menu(menu)

        # adds a command to the menu option, calling it undo, and the command that runs on event is clientExit
        # edit.add_command(label="Show Img", command=self.showImg)
        # edit.add_command(label="Show Text", command=self.showText)

        # added "edit" to our menu
        menu.add_cascade(label="Edit", menu=edit)

        # Creating a button instance
        quitButton = Button(self, text="Exit", command=self.clientExit)

        # Placeing the button in the window
        quitButton.place(x=0, y=0)

    # def showImg(self):
        # load = Image.open("chat.png")
        # render = ImageTk.PhotoImage(load)

        # labels can be text or images
        # img = Label(self, image=render)
        # img.image = render
        # img.place(x=20, y=0)

    # def showText(self):
        # text = Label(self, text="Hey there good lookin!")
        # text.pack()

    def clientExit(self):
        exit()

# creates the root window. rn that is the only window but later there could be windows within windows

root = Tk()
root.geometry("400x300")

# creation of an instance
app = Window(root)

# main loop
root.mainloop()