import random, pickle
from tkinter import *

BR = 715
BL = 605
DB = 415 
L = 580
DT = 250
TR = 1115
TL = 205
XC = BL + (BR - BL)/2
LFB = L + 80
YC = L/2

class Monster(object):
    def __init__(self, name, level, cords, canvas):
        self.name = name
        self.level = level
        self.initMonster()
        self.cords = cords
        self.drawMonster(canvas)
        self.escaped = False
        self.discovered = False

    def initMonster(self):
        self.attack = 4
        self.defence = 4
        self.agility = 4
        self.hpC = 4

        if self.level <= 3:
            self.loot = "50 gold"
        
        elif self.level >=3 and self.level <= 6:
            self.loot = "100 gold"

        else:
            self.loot = "150 gold"

        points = 10
        for lvl in range(1, 1 + self.level):
            points += (1*lvl)
        
        else:
            for pnt in range(points):
                stat = random.randint(1, 4)
                if stat == 1:
                    self.attack += 1

                elif stat == 2:
                    self.defence += 1

                elif stat == 3:
                    self.agility += 1

                elif stat == 4:
                    self.hpC += 1
                    self.hpM = self.hpC
    
    def drawMonster(self, canvas):
        if self.name == "Skelly":
            # draw skelleton 
            canvas.create_line(XC, L, XC, YC)
            canvas.create_oval(XC - XC/6, YC - YC/6, XC + XC/6, YC + YC/6)

        elif self.name == "Troll":
            # draw Troll
            pass

        elif self.name == "Slime":
            # draw Slime
            pass

        else:
            # draw Slime
            pass

    def pickleSaveMonster(self, fileName):
        pickleFile = open(fileName, 'wb')
        pickle.dump(self, pickleFile)
        pickleFile.close()

    def pickleLoadMonster(self, fileName):
        pickleFile = open(fileName, 'rb')
        monster = pickle.load(pickleFile)
        pickleFile.close()
        return monster

    # def print_stats(self):
    #     print ("lv{} {}".format(str(self.level), str(self.name)))
    #     print ("Hit points:{}/{}".format(str(self.hpC), str(self.hpM)))
    #     print ("Attack:    {}".format(str(self.attack)))
    #     print ("Defence:   {}".format(str(self.defence)))
    #     print ("Agility:   {}".format(str(self.agility)))

    # def hp_gen(self, difficultyLevel):
    #     #use difficulty multiplier and level to randomly generate hp
    #     if difficultyLevel == 1:
    #         hp = random.randint(14,17)

    #     elif difficultyLevel == 2:
    #         hp = random.randint(16,19)

    #     elif difficultyLevel == 3:
    #         hp = random.randint(18,21)

    #     elif difficultyLevel == 4:
    #         hp = random.randint(20,23)

    #     elif difficultyLevel == 5:
    #         hp = random.randint(22,25)

    #     else:
    #         hp = random.randint(18,21)

    #     return hp, hp

    # def att_gen(self, difficultyLevel):
    #     #use difficulty multiplier and level to randomly generate attack
    #     if difficultyLevel == 1:
    #         attack = random.randint(14,17)

    #     elif difficultyLevel == 2:
    #         attack = random.randint(16,19)

    #     elif difficultyLevel == 3:
    #         attack = random.randint(18,21)

    #     elif difficultyLevel == 4:
    #         attack = random.randint(20,23)

    #     elif difficultyLevel == 5:
    #         attack = random.randint(22,25)

    #     return attack

    # def def_gen(self, difficultyLevel):
    #     #use difficulty multiplier and level to randomly generate defence
    #     if difficultyLevel == 1:
    #         defence = random.randint(14,17)

    #     elif difficultyLevel == 2:
    #         defence = random.randint(16,19)

    #     elif difficultyLevel == 3:
    #         defence = random.randint(18,21)

    #     elif difficultyLevel == 4:
    #         defence = random.randint(20,23)

    #     elif difficultyLevel == 5:
    #         defence = random.randint(22,25)

    #     return defence

    # def agi_gen(self, difficultyLevel):
    #     #use difficulty multiplier and level to randomly generate agility
    #     if difficultyLevel == 1:
    #         agility = random.randint(14,17)

    #     elif difficultyLevel == 2:
    #         agility = random.randint(16,19)

    #     elif difficultyLevel == 3:
    #         agility = random.randint(18,21)

    #     elif difficultyLevel == 4:
    #         agility = random.randint(20,23)

    #     elif difficultyLevel == 5:
    #         agility = random.randint(22,25)

    #     return agility

    # def lootDrop(self, difficultyLevel):
    #     #use difficulty multiplier and level to randomly generate rewards
    #     #such as gold or items
    #     if difficultyLevel == 1:
    #         pass
    #     elif difficultyLevel == 2:
    #         pass
    #     elif difficultyLevel == 3:
    #         pass
    #     elif difficultyLevel == 4:
    #         pass
    #     elif difficultyLevel == 5:
    #         pass
    #     return ""