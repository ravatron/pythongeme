import tkinter
from tkinter import messagebox
from tkinter import *
import time, os, sys, re, pickle, random, copy
from characterClass import Character
# from mapGenerator import MapForGame
# from GameFinal import drawKnownMap

class GameWindow(Frame):
    
    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.master = master
        self.initWindow()

    def initWindow(self):
        self.master.title("Game Window")
        self.pack(fill=BOTH, expand=1)
        self.canvas = self.makeCanvas()
        self.mainMenuPage()
        self.master.resizable(1, 1)
        menu = Menu(self.master)
        self.master.config(menu=menu)
        file = Menu(menu)
        # file.add_command(label="Save", command=GameFinal.saveGameData)
        file.add_command(label="Quit", command=self.quitGame)
        menu.add_cascade(label="File", menu=file)
    
    # def saveGame(self, game):
    #     path = os.path.join("./SaveData", self.currentFile)
    #     openFile = open(path, "wb")
    #     pickle.dump(self, openFile)
    #     pickle.dump(game, openFile)
    #     openFile.close()

    def quitGame(self):
        exit()
    
    def newGamePage(self):
        self.canvas.delete(ALL)
        self.canvas.create_window(BL + (BR - BL)/2, DT + 100, window=Button(master=self.canvas, text="Start New Game", command=self.startnewgame))
        self.filenameEntry = self.makeEntry(self.canvas, "File Name: ", 15)
        self.canvas.create_window(BL + (BR - BL)/2, DT + 150, window=Button(master=self.canvas, text="Back to Main Menu", command=self.mainMenuPage))
    
    def mainMenuPage(self):
        self.canvas.delete(ALL)
        self.canvas.create_window(BL + (BR - BL)/2, DT, window=Label(master=self.canvas, text='WELCOME', font="Helvetica 40"))
        self.canvas.create_window(BL + (BR - BL)/2, DT + (DB - DT)/2, window=Button(master=self.master, text="New", font="Helvetica 20", command=self.newGamePage, pady=10, padx=50)) # make sure to not use "()" otherwise it will call the function once and stop
        self.canvas.create_window(BL + (BR - BL)/2, DT + (DB - DT)/2 + 40, window=Button(master=self.master, text="Load", font="Helvetica 20", command=self.loadGamePage, pady=10, padx=50))
        self.canvas.create_window(BL + (BR - BL)/2, DT + (DB - DT)/2 + 80, window=Button(master=self.master, text="Quit", font="Helvetica 20", command=self.quitGame, pady=10, padx=50))
    
    def loadGamePage(self):
        self.canvas.delete(ALL)
        saveFiles = os.listdir("./SaveData")
        listbox = Listbox(master=self.canvas, height=len(saveFiles), width=10)
        for saveFile in saveFiles:
            listbox.insert(END, saveFile.replace(".data", ""))
        self.canvas.create_window(BL + (BR - BL)/2, DT, window=listbox)
        self.selectedFile = listbox
        self.canvas.create_window(BL + (BR - BL)/2, DT + 30 + (len(saveFiles) * 5), window=Button(master=self.canvas, text="Load File", command=self.loadFile))
        self.canvas.create_window(BL + (BR - BL)/2, DT + 60 + (len(saveFiles) * 5), window=Button(master=self.canvas, text="Delete File", command=self.deleteFile))
        self.canvas.create_window(BL + (BR - BL)/2, DT + 150, window=Button(master=self.canvas, text="Back to Main Menu", command=self.mainMenuPage))

    def loadFile(self):
        filename = self.selectedFile.curselection()
        filename = str(self.selectedFile.get(filename)) + ".data"
        text = "Succesfully loaded %s" % (filename)
        self.succesfulTest(BL + (BR - BL)/2, DT/2, text)
        self.currentFile = filename

    def succesfulTest(self, x, y, message):
        if self.canvas.find_withtag("testMessage") != None:
            self.canvas.delete("testMessage")
        self.canvas.create_window(x, y, window=Label(master=self.canvas, text=message, fg="green", font="Hevelica 20"), tags="testMessage")
    
    def unsuccesfulTest(self, x, y, message):
        if self.canvas.find_withtag("testMessage") != None:
            self.canvas.delete("testMessage")
        self.canvas.create_window(x, y, window=Label(master=self.canvas, text=message, fg="red", font="Hevelica 20"), tags="testMessage")
    
    def deleteFile(self):
        fileIndex = self.selectedFile.curselection()
        filename = str(self.selectedFile.get(fileIndex)) + ".data"
        if messagebox.askyesno("Are You Sure?", "Are you sure you want to delete %s?" % str(self.selectedFile.get(fileIndex))) == True:
            self.selectedFile.delete(fileIndex)
            text = "Succesfully deleted %s" % (filename)
            os.remove(os.path.join("SaveData", filename))
            self.succesfulTest(BL + (BR - BL)/2, DT/2, text)
    
    def makeEntry(self, parent, caption, width=None, **options):
        entry = Entry(parent, **options)
        if width:
            entry.config(width=width)
        self.canvas.create_window(BL - (BR - BL)/2, DT, window=Label(parent, text=caption))
        self.canvas.create_window(BL + (BR - BL)/2, DT, window=entry)
        return entry

    # def makeTitle(self):
    #     label = self.canvas.create_window(BL + (BR - BL)/2, DT, window=Label(master=self.canvas, text='WELCOME', font="Helvetica 40"))
    #     return label
        
    # def makeTitleScreenButtons(self):
    #     self.canvas.create_window(BL + (BR - BL)/2, DT + (DB - DT)/2, window=Button(master=self.master, text="New", font="Helvetica 20", command=self.newGamePage, repeatdelay=2000, repeatinterval=2000, pady=10, padx=50)) # make sure to not use "()" otherwise it will call the function once and stop
    #     self.canvas.create_window(BL + (BR - BL)/2, DT + (DB - DT)/2 + 40, window=Button(master=self.master, text="Load", font="Helvetica 20", command=self.loadGamePage, repeatdelay=2000, repeatinterval=2000, pady=10, padx=50))
    #     self.canvas.create_window(BL + (BR - BL)/2, DT + (DB - DT)/2 + 80, window=Button(master=self.master, text="Quit", font="Helvetica 20", command=self.quitGame, repeatdelay=2000, repeatinterval=2000, pady=10, padx=50))

    def makeCanvas(self):
        canvas = ResizingCanvas(self.master, width=1325, height=785)
        canvas.pack(fill=BOTH, expand=1, in_=self.master)
        return canvas

    def startnewgame(self):
        filename = str(self.filenameEntry.get())
        filename.replace(" ", "")
        filename = str(filename) + ".data"
        path = "./SaveData/" + filename
        SaveData = os.listdir("./SaveData")
        if filename in SaveData:
            self.unsuccesfulTest(BL + (BR - BL)/2, DT/2, "That file already exists")
        else:
            self.succesfulTest(BL + (BR - BL)/2, DT/2, "File succesfully created")
            self.currentFile = filename
            self.currentFilePath = path
            aFile = open(path, "wb")
            aFile.close()
    
    def makeGameUI(self, directions):
        text1 = "You have come to an empty room"
        text2 = "Which direction will you go?"
        if len(directions) > 1:
            for x in range(len(directions)):
                if "forward" in directions:
                    self.canvas.create_window(XC, LFB, window=Button(master=self.canvas, text="Forward", command=self.forward, width=9))
                    directions.remove("Forward")
                
                elif "left" in directions:
                    self.canvas.create_window(XC - 60, LFB + 30, window=Button(master=self.canvas, text="Left", command=self.left, width=9))
                    directions.remove("Left")

                elif "right" in directions:
                    self.canvas.create_window(XC + 60, LFB + 30, window=Button(master=self.canvas, text="Right", command=self.right, width=9))
                    directions.remove("Right")
                
                elif "backward" in directions:
                    self.canvas.create_window(XC, LFB + 60, window=Button(master=self.canvas, text="Backward", command=self.backward, width=9))
                    directions.remove("Backward")

        else:
            
            self.canvas.create_window(XC, LFB, window=Button(master=self.canvas, text="Backward", command=self.backward, width=9))
        self.canvas.create_window(XC, L + 20, window=Label(master=self.canvas, text=text1, font="Hevelica 14"))
        self.canvas.create_window(XC, L + 40, window=Label(master=self.canvas, text=text2, font="Hevelica 14"))
        # self.canvas.create_window(TL, LFB, window=Button(master=self.canvas, text="Map", command=drawKnownMap))
        
    def forward(self):
        self.succesfulTest(XC, DT/2, "Forward button works")

    def backward(self):
        self.succesfulTest(XC, DT/2, "Backward button works")

    def left(self):
        self.succesfulTest(XC, DT/2, "Left button works")

    def right(self):
        self.succesfulTest(XC, DT/2, "Right button works")

# def drawKnownMap():
#     MapForGame.drawMap()

def pressedQuit():
    quit()

# def saveGameData():

class ResizingCanvas(Canvas):
    def __init__(self, parent, **kwargs):
        Canvas.__init__(self, parent, **kwargs)
        self.bind("<Configure>", self.on_resize)
        self.height = self.winfo_reqheight()
        self.width = self.winfo_reqwidth()

    def on_resize(self, event):
        wscale = float(event.width)/self.width
        hscale = float(event.height)/self.height
        self.width = event.width
        self.height = event.height
        self.config(width=self.width, height=self.height)
        self.scale("all", 0, 0, wscale, hscale)

BR = 715
BL = 605
DB = 415 
L = 580
DT = 250
TR = 1115
TL = 205
XC = BL + (BR - BL)/2
LFB = L + 80

# the room class
class Room(object):
    def __init__(self, top, bottom, left, right, player, treasure, x, y):
        # Directions where there are connections
        self.top = top
        self.bottom = bottom
        self.left = left
        self.right = right
        # Player will be it's own class that will be implemented later
        self.player = player
        # treasure will be items found in the room, might be their own class as well
        self.treasure = treasure
        # the x y cords of the room with top left being 0 and increasing by one to the right or down
        self.x = x
        self.y = y
        self.rn = self.test()

    def test(self):
        #This returns a number so the program can quickly identify the room it is working with
        #It is a series of true/false checks

        if self.top == True:

            if self.bottom == True:

                if self.left == True:

                    if self.right == True:
                        result = 11 # X shaped room

                    else:
                        result = 8 # T shaped room

                else:

                    if self.right == True:
                        result = 7 # T shaped room

                    else:
                        result = 1 # I shaped room

            else:

                if self.left == True:

                    if self.right == True:
                        result = 9 # T shaped room

                    else:
                        result = 4 # L shaped room

                else:

                    if self.right == True:
                        result = 3 # L shaped room

                    else:
                        result = 13 # dead end opening top

        else:

            if self.bottom == True:

                if self.left == True:

                    if self.right == True:
                        result = 10 # T shaped room

                    else:
                        result = 6 # L shaped room

                else:

                    if self.right == True:
                        result = 5 # L shaped room

                    else:
                        result = 12 # dead end opening bottom

            else:

                if self.left == True:

                    if self.right == True:
                        result = 2 # I shaped room

                    else:
                        result = 14 # dead end opening left

                else:

                    if self.right == True:
                        result = 15 # dead end opening right

                    else:
                        result = 16 # empty

        return result

    def test_direction(self):
        #This is so there is less code to write later on and just makes a dict saying weather there is a connection in a direction
        result = {}
        if self.top == True:
            result['top'] = True
        else:
            result['top'] = False
        if self.bottom == True:
            result['bottom'] = True
        else:
            result['bottom'] = False
        if self.left == True:
            result['left'] = True
        else:
            result['left'] = False
        if self.right == True:
            result['right'] = True
        else:
            result['right'] = False
        return result

    def drawRoomInterior(self, canvas, side=None):
        BR = 715
        BL = 605
        DB = 415 
        L = 580
        DT = 250
        TR = 1115
        TL = 205
        canvas.delete(ALL)
        canvas.create_line(0, L, 1600, L) # Base Line
        if self.rn == 1 or self.rn == 2: # I shaped rooms
            # Will have 1 state, strait ahead
            canvas.create_line(TL, L, BL, DB) # Bottom of left side
            canvas.create_line(TR, L, BR, DB) # Bottom of right side
            canvas.create_rectangle(BL, DT, BR, DB) # Doorway
            canvas.create_line(TL, 0, BL, DT) # Top of left side
            canvas.create_line(TR, 0, BR, DT) # Top of right side

        elif self.rn >= 3 and self.rn <= 6: # L shaped rooms
            # Will have 2 states: left, or right
            # TODO: figure out a way to tell which way the room is turning
            if side == "left":
                canvas.create_line(TL, L, TL + ((BL - TL)/3), L - ((L - DB)/3)) # bottom of left wall
                canvas.create_line(TL + ((BL - TL)/3), L - ((L - DB)/3), TL + ((BL - TL)/3), DT/3) # corner of left wall
                canvas.create_line(TL + ((BL - TL)/3), DT/3, TL, 0) # top of left wall
                canvas.create_line(TL + ((BL - TL)/3), DT - (DT/3), BR + (TR - BR)/3, DT - (DT/3)) # top of back wall
                canvas.create_line(TL + ((BL - TL)/3), DB + ((L - DB)/3), BR + (TR - BR)/3, DB + ((L - DB)/3)) # bottom of back wall
                canvas.create_line(BR + (TR - BR)/3, DB + ((L - DB)/3), BR + (TR - BR)/3, DT - (DT/3)) # corner of right wall
                canvas.create_line(BR + (TR - BR)/3, DB + ((L - DB)/3), TR, L) # bottom of right wall
                canvas.create_line(BR + (TR - BR)/3, DT - (DT/3), TR, 0) # top of right wall
            
            elif side == "right":
                canvas.create_line(TR - (TR - BR)/3, DT - (DT/3), BL - (BL - TL)/3, DT - (DT/3)) # top of back wall
                canvas.create_line(TR - (TR - BR)/3, DB + ((L - DB)/3), BL - (BL - TL)/3, DB + ((L - DB)/3)) # bottom of back wall
                canvas.create_line(TR - (TR - BR)/3, DT/3, TR, 0) # Top of right wall
                canvas.create_line(TR - (TR - BR)/3, DT/3, TR - (TR - BR)/3, L - (L - DB)/3) # Corner of right wall
                canvas.create_line(TR - (TR - BR)/3, L - (L - DB)/3, TR, L) # Bottom of right wall
                canvas.create_line(BL - (BL - TL)/3, DT - (DT/3), TL, 0) # Top of left wall
                canvas.create_line(BL - (BL - TL)/3, DT - (DT/3), BL - (BL - TL)/3, DB + (L - DB)/3) # Corner of left wall
                canvas.create_line(BL - (BL - TL)/3, DB + (L - DB)/3, TL, L) # Bottom of left wall

        elif self.rn >= 7 and self.rn <= 10: # T shaped rooms
            # Will have 3 states: left, right, and None
            # TODO: figure out a way to tell which state to enter
            if side == "left":
                canvas.create_line(TL, L, TL + ((BL - TL)/3), L - ((L - DB)/3)) # Front Bottom of left wall
                canvas.create_line(TL + ((BL - TL)/3), DT/3, TL, 0) # Front Top of left wall
                canvas.create_line(TL + ((BL - TL)/3), L - ((L - DB)/3), TL + ((BL - TL)/3), DT/3) # Front Corner of left wall
                canvas.create_line(BL - (BL - TL)/3, DT - (DT/3), BL - (BL - TL)/3, DB + (L - DB)/3) # Back Corner of left wall
                canvas.create_line(BL - (BL - TL)/3, DB + (L - DB)/3, BL, DB) # Back Bottom of left wall
                canvas.create_line(BL - (BL - TL)/3, DT - (DT/3), BL, DT) # Back Top of left wall
                canvas.create_line(TR, L, BR, DB) # Bottom of right wall
                canvas.create_line(TR, 0, BR, DT) # Top of right wall
                canvas.create_rectangle(BL, DT, BR, DB) # Doorway
                canvas.create_line(TL + (BL - TL)/3, DT - (DT/3), BL - (BL - TL)/3, DT - (DT/3)) # Top of Left hallway wall
                canvas.create_line(TL + (BL - TL)/3, DB + (L - DB)/3, BL - (BL - TL)/3, DB + ((L - DB)/3)) # Bottom of left hallway wall

            elif side == "right":
                canvas.create_line(TR - (TR - BR)/3, DT - (DT/3), BR + (BL - TL)/3, DT - (DT/3)) # top of back wall
                canvas.create_line(TR - (TR - BR)/3, DB + (L - DB)/3, BR + (BL - TL)/3, DB + (L - DB)/3) # bottom of back wall
                canvas.create_line(TR - (TR - BR)/3, DT/3, TR, 0) # Top of right wall
                canvas.create_line(TR - (TR - BR)/3, L - (L - DB)/3, TR, L) # Bottom of right wall
                canvas.create_line(TR - (TR - BR)/3, DT/3, TR - (TR - BR)/3, L - (L - DB)/3) # Front Corner of right wall
                canvas.create_line(BR + (TR - BR)/3, DB + (L - DB)/3, BR + (TR - BR)/3, DT - DT/3) # Back corner of right wall
                canvas.create_line(TL, L, BL, DB) # Bottom of left side
                canvas.create_line(TL, 0, BL, DT) # Top of left side
                canvas.create_rectangle(BL, DT, BR, DB) # Doorway
                canvas.create_line(BR + (TR - BR)/3, DB + (L - DB)/3, BR, DB) # Back Bottom of right side
                canvas.create_line(BR + (TR - BR)/3, DT - DT/3, BR, DT) # Top of right side
            
            else:
                canvas.create_line(TL, L, TL + ((BL - TL)/3), L - ((L - DB)/3)) # bottom of left wall
                canvas.create_line(TL + ((BL - TL)/3), L - ((L - DB)/3), TL + ((BL - TL)/3), DT/3) # corner of left wall
                canvas.create_line(TL + ((BL - TL)/3), DT/3, TL, 0) # top of left wall
                canvas.create_line(TR - (TR - BR)/3, DT - (DT/3), TL + (BL - TL)/3, DT - (DT/3)) # top of back wall
                canvas.create_line(TR - (TR - BR)/3, DB + (L - DB)/3, TL + (BL - TL)/3, DB + ((L - DB)/3)) # bottom of back wall
                canvas.create_line(TR - (TR - BR)/3, DT/3, TR, 0) # Top of right wall
                canvas.create_line(TR - (TR - BR)/3, DT/3, TR - (TR - BR)/3, L - (L - DB)/3) # Corner of right wall
                canvas.create_line(TR - (TR - BR)/3, L - (L - DB)/3, TR, L) # Bottom of right wall

        elif self.rn == 11: # X shaped room
            # Will have 1 state: forward and left and right
            canvas.create_rectangle(BL, DT, BR, DB) # Doorway
            canvas.create_line(TL, L, TL + ((BL - TL)/3), L - ((L - DB)/3)) # Front Bottom of left wall
            canvas.create_line(TL + ((BL - TL)/3), DT/3, TL, 0) # Front Top of left wall
            canvas.create_line(TL + ((BL - TL)/3), L - ((L - DB)/3), TL + ((BL - TL)/3), DT/3) # Front Corner of left wall
            canvas.create_line(BL - (BL - TL)/3, DT - (DT/3), BL - (BL - TL)/3, DB + (L - DB)/3) # Back Corner of left wall
            canvas.create_line(BL - (BL - TL)/3, DB + (L - DB)/3, BL, DB) # Back Bottom of left wall
            canvas.create_line(BL - (BL - TL)/3, DT - (DT/3), BL, DT) # Back Top of left wall
            canvas.create_line(TL + (BL - TL)/3, DT - (DT/3), BL - (BL - TL)/3, DT - (DT/3)) # Top of Left hallway wall
            canvas.create_line(TL + (BL - TL)/3, DB + (L - DB)/3, BL - (BL - TL)/3, DB + ((L - DB)/3)) # Bottom of left hallway wall
            canvas.create_line(TR - (TR - BR)/3, DT - (DT/3), BR + (BL - TL)/3, DT - (DT/3)) # top of back wall
            canvas.create_line(TR - (TR - BR)/3, DB + (L - DB)/3, BR + (BL - TL)/3, DB + (L - DB)/3) # bottom of back wall
            canvas.create_line(TR - (TR - BR)/3, DT/3, TR, 0) # Top of right wall
            canvas.create_line(TR - (TR - BR)/3, L - (L - DB)/3, TR, L) # Bottom of right wall
            canvas.create_line(TR - (TR - BR)/3, DT/3, TR - (TR - BR)/3, L - (L - DB)/3) # Front Corner of right wall
            canvas.create_line(BR + (TR - BR)/3, DB + (L - DB)/3, BR + (TR - BR)/3, DT - DT/3) # Back corner of right wall
            canvas.create_line(BR + (TR - BR)/3, DB + (L - DB)/3, BR, DB) # Back Bottom of right side
            canvas.create_line(BR + (TR - BR)/3, DT - DT/3, BR, DT) # Top of right side
        
        elif self.rn >= 12 and self.rn <= 15: # Dead ends
            # Will have 1 state: None
            canvas.create_line(TL, L, TL + TL/4, L - (L - DB)/4, TL + TL/4, DT/4, TL, 0) # Front Left corner
            canvas.create_line(TR, L, TR - TL/4, L - (L - DB)/4, TR - TL/4, DT/4, TR, 0) # Front Right corner
            canvas.create_line(TL + TL/4, DB + 2*(L - DB)/5, TL + 3*TL/5, DB + (L - DB)/8, TR - 3*TL/5, DB + (L - DB)/8, TR - TL/4, DB + 2*(L - DB)/5) # Bottom Back of room
            canvas.create_line(TL + 3*TL/5, DB + (L - DB)/8, TL + 3*TL/5, DT - DT/4, TL + TL/4, DT - 2.75*DT/5) # Back Left corner
            canvas.create_line(TR - 3*TL/5, DB + (L - DB)/8, TR - 3*TL/5, DT - DT/4, TR - TL/4, DT - 2.75*DT/5) # Back Right corner
            canvas.create_line(TL + 3*TL/5, DT - DT/4, TR - 3*TL/5, DT - DT/4) # Top of back wall

        canvas.addtag_all("currentRoom")
        tag = "currentRoom"
        return tag 

    def drawPlayer(self, canvas):
        x = self.x * 60
        y = self.y * 60
        canvas.create_polygon(x + 30, y + 20, x + 35, y + 35, x + 30, y + 30, x + 25, y + 35, tags="player")

    def drawRoomExterior(self, canvas):
        #this draws the room and returns the image so it can be deleted later on if need be
        if self.player == True:
            self.drawPlayer(canvas)
        x1 = self.x * 60
        y1 = self.y * 60
        if self.rn == 1:
            x1 += 10
            x2 = x1 + 40
            y2 = y1 + 60
            a = canvas.create_line(x1, y1, x1, y2)
            b = canvas.create_line(x2, y1, x2, y2)
            shape = [a, b]
        elif self.rn == 2:
            x2 = x1 + 60
            y1 += 10
            y2 = y1 + 40
            a = canvas.create_line(x1, y1, x2, y1)
            b = canvas.create_line(x1, y2, x2, y2)
            shape = [a, b]
        elif self.rn == 3:
            x1 += 10
            x2 = x1 + 40
            y2 = y1 + 10
            x3 = x2 + 10
            y3 = y2 + 40
            a = canvas.create_line(x1, y1, x1, y3)
            b = canvas.create_line(x1, y3, x3, y3)
            c = canvas.create_line(x2, y1, x2, y2)
            d = canvas.create_line(x2, y2, x3, y2)
            shape = [a, b, c, d]
        elif self.rn == 4:
            x2 = x1 + 10
            x3 = x2 + 40
            y2 = y1 + 10
            y3 = y2 + 40
            a = canvas.create_line(x1, y2, x2, y2)
            b = canvas.create_line(x2, y1, x2, y2)
            c = canvas.create_line(x1, y3, x3, y3)
            d = canvas.create_line(x3, y3, x3, y1)
            shape = [a, b, c, d]
        elif self.rn == 5:
            x1 += 10
            x2 = x1 + 40
            x3 = x2 + 10
            y1 += 10
            y2 = y1 + 40
            y3 = y2 + 10
            a = canvas.create_line(x1, y1, x1, y3)
            b = canvas.create_line(x1, y1, x3, y1)
            c = canvas.create_line(x2, y2, x3, y2)
            d = canvas.create_line(x2, y2, x2, y3)
            shape = [a, b, c, d]
        elif self.rn == 6:
            x2 = x1 + 10
            x3 = x2 + 40
            y1 += 10
            y2 = y1 + 40
            y3 = y2 + 10
            a = canvas.create_line(x1, y1, x3, y1)
            b = canvas.create_line(x3, y1, x3, y3)
            c = canvas.create_line(x1, y2, x2, y2)
            d = canvas.create_line(x2, y2, x2, y3)
            shape = [a, b, c, d]
        elif self.rn == 7:
            x1 += 10
            x2 = x1 + 40
            x3 = x2 + 10
            y2 = y1 + 10
            y3 = y2 + 40
            y4 = y3 + 10
            a = canvas.create_line(x1, y1, x1, y4)
            b = canvas.create_line(x2, y1, x2, y2)
            c = canvas.create_line(x2, y2, x3, y2)
            d = canvas.create_line(x2, y3, x3, y3)
            e = canvas.create_line(x2, y3, x2, y4)
            shape = [a, b, c, d, e]
        elif self.rn == 8:
            x2 = x1 + 10
            x3 = x2 + 40
            y2 = y1 + 10
            y3 = y2 + 40
            y4 = y3 + 10
            a = canvas.create_line(x3, y1, x3, y4)
            b = canvas.create_line(x2, y1, x2, y2)
            c = canvas.create_line(x1, y2, x2, y2)
            d = canvas.create_line(x1, y3, x2, y3)
            e = canvas.create_line(x2, y3, x2, y4)
            shape = [a, b, c, d, e]
        elif self.rn == 9:
            x2 = x1 + 10
            x3 = x2 + 40
            x4 = x3 + 10
            y2 = y1 + 10
            y3 = y2 + 40
            a = canvas.create_line(x1, y3, x4, y3)
            b = canvas.create_line(x1, y2, x2, y2)
            c = canvas.create_line(x2, y1, x2, y2)
            d = canvas.create_line(x3, y1, x3, y2)
            e = canvas.create_line(x3, y2, x4, y2)
            shape = [a, b, c, d, e]
        elif self.rn == 10:
            x2 = x1 + 10
            x3 = x2 + 40
            x4 = x3 + 10
            y1 += 10
            y2 = y1 + 40
            y3 = y2 + 10
            a = canvas.create_line(x1, y1, x4, y1)
            b = canvas.create_line(x1, y2, x2, y2)
            c = canvas.create_line(x2, y2, x2, y3)
            d = canvas.create_line(x3, y2, x3, y3)
            e = canvas.create_line(x3, y2, x4, y2)
            shape = [a, b, c, d, e]
        elif self.rn == 11:
            x2 = x1 + 10
            x3 = x2 + 40
            x4 = x3 + 10
            y2 = y1 + 10
            y3 = y2 + 40
            y4 = y3 + 10
            a = canvas.create_line(x2, y1, x2, y2)
            b = canvas.create_line(x3, y1, x3, y2)
            c = canvas.create_line(x1, y2, x2, y2)
            d = canvas.create_line(x3, y2, x4, y2)
            e = canvas.create_line(x1, y3, x2, y3)
            f = canvas.create_line(x3, y3, x4, y3)
            g = canvas.create_line(x2, y3, x2, y4)
            h = canvas.create_line(x3, y3, x3, y4)
            shape = [a, b, c, d, e, f, g, h]
        elif self.rn == 12:
            x1 += 5
            x2 = x1 + 5
            x3 = x2 + 40
            x4 = x3 + 5
            y1 += 5
            y2 = y1 + 50
            y3 = y2 + 10
            a = canvas.create_line(x1, y1, x4, y1)
            b = canvas.create_line(x1, y1, x1, y2)
            c = canvas.create_line(x4, y1, x4, y2)
            d = canvas.create_line(x1, y2, x2, y2)
            e = canvas.create_line(x2, y2, x2, y3)
            f = canvas.create_line(x3, y2, x4, y2)
            g = canvas.create_line(x3, y2, x3, y3)
            shape = [a, b, c, d, e, f, g]
        elif self.rn == 13:
            x1 += 5
            x2 = x1 + 5
            x3 = x2 + 40
            x4 = x3 + 5
            y2 = y1 + 5
            y3 = y2 + 50
            a = canvas.create_line(x1, y3, x4, y3)
            b = canvas.create_line(x1, y2, x1, y3)
            c = canvas.create_line(x4, y2, x4, y3)
            d = canvas.create_line(x1, y2, x2, y2)
            e = canvas.create_line(x2, y1, x2, y2)
            f = canvas.create_line(x3, y1, x3, y2)
            g = canvas.create_line(x3, y2, x4, y2)
            shape = [a, b, c, d, e, f, g]
        elif self.rn == 14:
            x2 = x1 + 5
            x3 = x2 + 50
            y1 += 5
            y2 = y1 + 5
            y3 = y2 + 40
            y4 = y3 + 5
            a = canvas.create_line(x1, y2, x2, y2)
            b = canvas.create_line(x2, y2, x2, y1)
            c = canvas.create_line(x2, y1, x3, y1)
            d = canvas.create_line(x3, y1, x3, y4)
            e = canvas.create_line(x3, y4, x2, y4)
            f = canvas.create_line(x2, y4, x2, y3)
            g = canvas.create_line(x2, y3, x1, y3)
            shape = [a, b, c, d, e, f, g]
        elif self.rn == 15:
            x1 += 5
            x2 = x1 + 50
            x3 = x2 + 10
            y1 += 5
            y2 = y1 + 5
            y3 = y2 + 40
            y4 = y3 + 5
            a = canvas.create_line(x3, y2, x2, y2)
            b = canvas.create_line(x2, y2, x2, y1)
            c = canvas.create_line(x2, y1, x1, y1)
            d = canvas.create_line(x1, y1, x1, y4)
            e = canvas.create_line(x1, y4, x2, y4)
            f = canvas.create_line(x2, y4, x2, y3)
            g = canvas.create_line(x2, y3, x3, y3)
            shape = [a, b, c, d, e, f, g]
        else:
            x2 = x1 + 60
            y2 = y1 + 60
            a = canvas.create_rectangle(x1, y1, x2, y2, fill='white', outline='white')
            shape = [a]
        return shape
    
#Basic rooms to build the framework of the map
frame_room = Room(False, False, False, False, False, False, 0, 0)
rooms = Room(True, True, True, True, False, False, 0, 0)
class MapForGame(object):
    def __init__(self, maxX=22, maxY=13):
        self.gameMap = self.finishMap(maxX, maxY)
        

    #builds the framework of the map
    def makeArray(self, max_x, max_y):
        #the empty frame
        array = []
        
        #Makes sub-lists for each room in y

        for y in range(max_y + 2):
            array.append([frame_room])
        #adds x amount of rooms to the width of the map and puts a frame on the end

        for y in range(1, max_y + 1):

            for x in range(1, max_x + 1):
                array[y].append(rooms)
            array[y].append(frame_room)
        #adds a frame to the top and the bottom of the framework

        for x in range(max_x + 1):
            array[0].append(frame_room)
            array[max_y + 1].append(frame_room)

        return array
    
    def makeMap(self, max_x, max_y):
        #the algorithem that builds the map
        # z = 0.67 #the chance for a connection to be made to the bottom or the right of any room
        z = float(random.randint(35, 75) / 100)
        map_frame = self.makeArray(max_x, max_y) #creats a framework for the map to be put in
        
        #runs once for every entry in the map frame

        for y in range(len(map_frame)):

            for x in range(len(map_frame[y])):

                if map_frame[y][x] != frame_room: #checks to make sure that it is not creating a room on the border of the map
                #similar to the room test in exicution in that it is a serise of true - false statements

                    if map_frame[y - 1][x].bottom == False:

                        if map_frame[y][x - 1].right == False:

                            if map_frame[y][x + 1].left == False:

                                if map_frame[y + 1][x].top == False:
                                    map_frame[y][x] = Room(False, False, False, False, False, False, x, y)

                                else:
                                    ynb = 0.01 * float(random.randint(1, 100)) #random chance is done like this so I can change the sensitivity if I want, say I want 67.5% chance

                                    if ynb <= z:
                                        map_frame[y][x] = Room(False, True, False, False, False, False, x, y)

                                    else:
                                        map_frame[y][x] = Room(False, False, False, False, False, False, x, y)

                            else:
                                ynr = 0.01 * float(random.randint(1, 100))

                                if map_frame[y + 1][x].top == False:

                                    if ynr <= z:
                                        map_frame[y][x] = Room(False, False, False, True, False, False, x, y)
                                   
                                    else:
                                        map_frame[y][x] = Room(False, False, False, False, False, False, x, y)
             
                                else:
                                    ynd = 0.01 * float(random.randint(1, 100))

                                    if ynr <= z:

                                        if ynd <= z:
                                            map_frame[y][x] = Room(False, True, False, True, False, False, x, y)

                                        else:
                                            map_frame[y][x] = Room(False, False, False, True, False, False, x, y)

                                    else:

                                        if ynd <= z:
                                            map_frame[y][x] = Room(False, True, False, False, False, False, x, y)

                                        else:
                                            map_frame[y][x] = Room(False, False, False, False, False, False, x, y)

                        else:

                            if map_frame[y][x + 1].left == False:

                                if map_frame[y + 1][x].top == False:
                                    map_frame[y][x] = Room(False, False, True, False, False, False, x, y)

                                else:
                                    ynb = 0.01 * float(random.randint(1, 100))

                                    if ynb <= z:
                                        map_frame[y][x] = Room(False, True, True, False, False, False, x, y)

                                    else:
                                         map_frame[y][x] = Room(False, False, True, False, False, False, x, y)

                            else:
                                ynr = 0.01 * float(random.randint(1, 100))

                                if map_frame[y + 1][x].top == False:

                                    if ynr <= z:
                                        map_frame[y][x] = Room(False, False, True, True, False, False, x, y)

                                    else:
                                        map_frame[y][x] = Room(False, False, True, False, False, False, x, y)

                                else:
                                    ynd = 0.01 * float(random.randint(1, 100))

                                    if ynr <= z:

                                        if ynd <= z:
                                            map_frame[y][x] = Room(False, True, True, True, False, False, x, y)

                                        else:
                                            map_frame[y][x] = Room(False, False, True, True, False, False, x, y)

                                    else:

                                        if ynd <= z:
                                            map_frame[y][x] = Room(False, True, True, False, False, False, x, y)

                                        else:
                                            map_frame[y][x] = Room(False, False, True, False, False, False, x, y)

                    else:

                        if map_frame[y][x - 1].right == False:

                            if map_frame[y][x + 1].left == False:

                                if map_frame[y + 1][x].top == False:
                                    map_frame[y][x] = Room(True, False, False, False, False, False, x, y)

                                else:
                                    ynb = 0.01 * float(random.randint(1, 100))

                                    if ynb <= z:
                                        map_frame[y][x] = Room(True, True, False, False, False, False, x, y)

                                    else:
                                        map_frame[y][x] = Room(True, False, False, False, False, False, x, y)

                            else:
                                ynr = 0.01 * float(random.randint(1, 100))

                                if map_frame[y + 1][x].top == False:

                                    if ynr <= z:
                                        map_frame[y][x] = Room(True, False, False, True, False, False, x, y)

                                    else:
                                        map_frame[y][x] = Room(True, False, False, False, False, False, x, y)

                                else:
                                    ynd = 0.01 * float(random.randint(1, 100))

                                    if ynr <= z:

                                        if ynd <= z:
                                            map_frame[y][x] = Room(True, True, False, True, False, False, x, y)

                                        else:
                                            map_frame[y][x] = Room(True, False, False, True, False, False, x, y)

                                    else:

                                        if ynd <= z:
                                            map_frame[y][x] = Room(True, True, False, False, False, False, x, y)

                                        else:
                                            map_frame[y][x] = Room(True, False, False, False, False, False, x, y)

                        else:

                            if map_frame[y][x + 1].left == False:

                                if map_frame[y + 1][x].top == False:
                                    map_frame[y][x] = Room(True, False, True, False, False, False, x, y)

                                else:
                                    ynb = 0.01 * float(random.randint(1, 100))

                                    if ynb <= z:
                                        map_frame[y][x] = Room(True, True, True, False, False, False, x, y)

                                    else:
                                        map_frame[y][x] = Room(True, False, True, False, False, False, x, y)

                            else:
                                ynr = 0.01 * float(random.randint(1, 100))

                                if map_frame[y + 1][x] == 1 or map_frame[y + 1][x].top == False:

                                    if ynr <= z:
                                        map_frame[y][x] = Room(True, False, True, True, False, False, x, y)

                                    else:
                                        map_frame[y][x] = Room(True, False, True, False, False, False, x, y)

                                else:
                                    ynd = 0.01 * float(random.randint(1, 100))

                                    if ynr <= z:

                                        if ynd <= z:
                                            map_frame[y][x] = Room(True, True, True, True, False, False, x, y)

                                        else:
                                            map_frame[y][x] = Room(True, False, True, True, False, False, x, y)

                                    else:

                                        if ynd <= z:
                                            map_frame[y][x] = Room(True, True, True, False, False, False, x, y)

                                        else:
                                            map_frame[y][x] = Room(True, False, True, False, False, False, x, y)

        return map_frame

    #draws the map room by room and returns a map of shapes so I could delete a specific room if I know the x y cords
    def drawMap(self, canvas, map_frame):
        canvas.delete(ALL)
        b = len(map_frame) - 2
        a = len(map_frame[0]) - 2
        shapes = self.makeArray(a, b)
    
        for y in range(len(map_frame)):
    
            for x in range(len(map_frame[y])):
                shapes[y][x] = map_frame[y][x].drawRoomExterior(canvas)
    
        return shapes
    
    #tests the map to see if the size of the room set is large enough (Formula for that is still being tweaked)
    def testMap(self, map_frame):
        frame = copy.deepcopy(map_frame) #makes a deep copy of the map to edit so the program can count the room sets
        set = False
        set_num = 0
        sets = {}
        frame_room = Room(False, False, False, False, False, False, 0, 0)
      
        for y in range(len(frame)):

            for x in range(len(frame[y])):

                if frame[y][x].top == True or frame[y][x].bottom == True or frame[y][x].left == True or frame[y][x].right == True: #checks every room to see if it is a new set or if it is a edge peice/old set
                    a = y
                    b = x
                    set = True
                    set_num += 1
                    set_size = 0
                    fork_rooms = []
                    counted_rooms = []
                    # count = 0

                    while set == True: #starts to count the set
                        # count += 1 #used for bug testing
                        #works by creating a 'counter' that goes room by room and adds the room to a list of counted rooms
                        c = frame[a][b].test_direction() #gives a list of connections for the room the counter is in
                        t = frame[a - 1][b].test_direction() #gives a list of connections for the room above the counter
                        d = frame[a + 1][b].test_direction() #gives a list of connections for the room below the counter
                        l = frame[a][b - 1].test_direction() #gives a list of connections for the room left of the counter
                        r = frame[a][b + 1].test_direction() #gives a list of connections for the room right of the counter

                        if c['top'] == True and t['bottom'] == True:
                            #moves the counter up
                            #checks to see if there are more than one connections

                            if (c['bottom'] == True and d['top'] == True) or (c['left'] == True and l['right'] == True) or (c['right'] == True and r['left'] == True):
                                #there is so the counter adds the room to a list of fork rooms to return to later
                                frame[a][b].top = False
                                frame[a - 1][b].bottom = False
                                fork_rooms.append(frame[a][b])

                            else:
                                #there isn't so the counter adds it to the counted rooms list and turns the room into a frame room so it won't get counted again
                                counted_rooms.append(frame[a][b])
                                frame[a][b] = copy.deepcopy(frame_room)
                                frame[a][b].y = a
                                frame[a][b].x = b
                                set_size += 1
                            a -= 1

                        elif c['bottom'] == True and d['top'] == True:
                            #moves the counter down

                            if (c['left'] == True and l['right'] == True) or (c['right'] == True and r['left'] == True):
                                frame[a][b].bottom = False
                                frame[a + 1][b].top = False
                                fork_rooms.append(frame[a][b])

                            else:
                                counted_rooms.append(frame[a][b])
                                frame[a][b] = copy.deepcopy(frame_room)
                                frame[a][b].y = a
                                frame[a][b].x = b
                                set_size += 1
                            a += 1

                        elif c['left'] == True and l['right'] == True:
                            #moves the counter left

                            if c['right'] == True and r['left'] == True:
                                frame[a][b].left = False
                                frame[a][b - 1].right = False
                                fork_rooms.append(frame[a][b])

                            else:
                                counted_rooms.append(frame[a][b])
                                frame[a][b] = copy.deepcopy(frame_room)
                                frame[a][b].y = a
                                frame[a][b].x = b
                                set_size += 1
                            b -= 1

                        elif c['right'] == True and r['left'] == True:
                            #moves the counter right
                            counted_rooms.append(frame[a][b])
                            frame[a][b + 1].left = False
                            frame[a][b] = copy.deepcopy(frame_room)
                            frame[a][b].y = a
                            frame[a][b].x = b
                            set_size += 1
                            b += 1

                        else: #this happens when the counter reaches a dead end

                            if len(fork_rooms) > 0:
                                #the counter adds the room it is in then moves back to the last split
                                check = False
                                counted_rooms.append(frame[a][b])
                                frame[a][b] = copy.deepcopy(frame_room)
                                frame[a][b].y = a
                                frame[a][b].x = b

                                while check == False and len(fork_rooms) > 0:
                                    z = fork_rooms.pop() #removes the latest entry to the fork room list and checks to see that it hasn't already been counted
                                    a = z.y
                                    b = z.x

                                    for g in counted_rooms:

                                        if g.x == z.x and g.y == z.y:
                                            #if it is then it breaks the for loop and runs the while loop as long as there are more entries in the fork room list
                                            check = False
                                            break

                                        else:
                                            #if the fork is not already counted then the while loop finishes
                                            check = True

                                else:

                                    if check == True: #if the while loop finished because there is an uncounted fork it adds it and runs from the fork
                                        set_size += 1

                                    elif len(fork_rooms) == 0: #if the while loop finished because there are no more forks then it adds the room it is in then ends the set
                                        set_size += 1
                                        sets[set_num] = counted_rooms
                                        set = False
    
                            else:
                                counted_rooms.append(frame[a][b])
                                frame[a][b] = copy.deepcopy(frame_room)
                                frame[a][b].y = a
                                frame[a][b].x = b
                                set_size += 1
                                sets[set_num] = counted_rooms
                                set = False
                                #ends the set

        return sets
    
    def finishMap(self, max_x, max_y): #function to make the final version of the map
        check = False

        while check == False:
            first_map = self.makeMap(max_x, max_y) #creates a map and tests it
            sets = self.testMap(first_map)

            if len(sets) > 1:
                largest_set = 1

                for x in sets:

                    if len(sets[x]) > len(sets[largest_set]): #finds the largest set in a map
                        largest_set = x

                    else:
                        continue

            else:
                largest_set = 1
            largest_set = sets[largest_set]

            if len(largest_set) < (max_x * max_y) - (2 * (max_x + max_y)) or len(largest_set) > (max_x * max_y) - (max_x + max_y): #checks to see if the map is large enough
                check = False

            else:
                check = True

        else:
            final_map = self.makeArray(max_x, max_y) #makes the frame for the final map

            for a in range(len(first_map)):

                for b in range(len(first_map[a])):

                    for room in largest_set:

                        if room.x == b and room.y == a: #makes the map with only the largest set of rooms
                            final_map[a][b] = copy.deepcopy(first_map[a][b])
                            break

                        else:
                            continue
            self.findStart(final_map)
            return final_map
        
    def findStart(self, finalMap):
        self.startRoom = None
        for y in range(len(finalMap)):
            for x in range(len(finalMap)):
                if finalMap[y][x].rn >= 12 and finalMap[y][x].rn <= 15:
                    self.startRoom = finalMap[y][x]
                    break
                if self.startRoom:
                    break
            if self.startRoom:
                break

# root = Tk()
# root.geometry("1320x780")
# root.minsize(width=990, height=585)

# app = GameWindow(root)
# root.mainloop()

# commands for making graphics for rooms:

    # canvas.create_line(0, L, 1320, L) # Base Line
   
    # I shaped:
   
    # canvas.create_line(TL, L, BL, DB) # Bottom of left side
    # canvas.create_line(TR, L, BR, DB) # Bottom of right side
    # canvas.create_rectangle(BL, DT, BR, DB) # Doorway
    # canvas.create_line(TL, 0, BL, DT) # Top of left side
    # canvas.create_line(TR, 0, BR, DT) # Top of right side
   
    # L shaped to the left:
   
    # canvas.create_line(TL, L, TL + ((BL - TL)/3), L - ((L - DB)/3)) # bottom of left wall
    # canvas.create_line(TL + ((BL - TL)/3), L - ((L - DB)/3), TL + ((BL - TL)/3), DT/3) # corner of left wall
    # canvas.create_line(TL + ((BL - TL)/3), DT/3, TL, 0) # top of left wall
    # canvas.create_line(TL + ((BL - TL)/3), DT - (DT/3), BR + (TR - BR)/3, DT - (DT/3)) # top of back wall
    # canvas.create_line(TL + ((BL - TL)/3), DB + ((L - DB)/3), BR + (TR - BR)/3, DB + ((L - DB)/3)) # bottom of back wall
    # canvas.create_line(BR + (TR - BR)/3, DB + ((L - DB)/3), BR + (TR - BR)/3, DT - (DT/3)) # corner of right wall
    # canvas.create_line(BR + (TR - BR)/3, DB + ((L - DB)/3), TR, L) # bottom of right wall
    # canvas.create_line(BR + (TR - BR)/3, DT - (DT/3), TR, 0) # top of right wall
   
    # L shaped to the right:
   
    # canvas.create_line(TR - (TR - BR)/3, DT - (DT/3), BL - (BL - TL)/3, DT - (DT/3)) # top of back wall
    # canvas.create_line(TR - (TR - BR)/3, DB + ((L - DB)/3), BL - (BL - TL)/3, DB + ((L - DB)/3)) # bottom of back wall
    # canvas.create_line(TR - (TR - BR)/3, DT/3, TR, 0) # Top of right wall
    # canvas.create_line(TR - (TR - BR)/3, DT/3, TR - (TR - BR)/3, L - (L - DB)/3) # Corner of right wall
    # canvas.create_line(TR - (TR - BR)/3, L - (L - DB)/3, TR, L) # Bottom of right wall
    # canvas.create_line(BL - (BL - TL)/3, DT - (DT/3), TL, 0) # Top of left wall
    # canvas.create_line(BL - (BL - TL)/3, DT - (DT/3), BL - (BL - TL)/3, DB + (L - DB)/3) # Corner of left wall
    # canvas.create_line(BL - (BL - TL)/3, DB + (L - DB)/3, TL, L) # Bottom of left wall
   
    # T shaped to the left:
   
    # canvas.create_line(TL, L, TL + ((BL - TL)/3), L - ((L - DB)/3)) # Front Bottom of left wall
    # canvas.create_line(TL + ((BL - TL)/3), DT/3, TL, 0) # Front Top of left wall
    # canvas.create_line(TL + ((BL - TL)/3), L - ((L - DB)/3), TL + ((BL - TL)/3), DT/3) # Front Corner of left wall
    # canvas.create_line(BL - (BL - TL)/3, DT - (DT/3), BL - (BL - TL)/3, DB + (L - DB)/3) # Back Corner of left wall
    # canvas.create_line(BL - (BL - TL)/3, DB + (L - DB)/3, BL, DB) # Back Bottom of left wall
    # canvas.create_line(BL - (BL - TL)/3, DT - (DT/3), BL, DT) # Back Top of left wall
    # canvas.create_line(TR, L, BR, DB) # Bottom of right wall
    # canvas.create_line(TR, 0, BR, DT) # Top of right wall
    # canvas.create_rectangle(BL, DT, BR, DB) # Doorway
    # canvas.create_line(TL + (BL - TL)/3, DT - (DT/3), BL - (BL - TL)/3, DT - (DT/3)) # Top of Left hallway wall
    # canvas.create_line(TL + (BL - TL)/3, DB + (L - DB)/3, BL - (BL - TL)/3, DB + ((L - DB)/3)) # Bottom of left hallway wall
    
    # T shaped to the right:
    
    # canvas.create_line(TR - (TR - BR)/3, DT - (DT/3), BR + (BL - TL)/3, DT - (DT/3)) # top of back wall
    # canvas.create_line(TR - (TR - BR)/3, DB + (L - DB)/3, BR + (BL - TL)/3, DB + (L - DB)/3) # bottom of back wall
    # canvas.create_line(TR - (TR - BR)/3, DT/3, TR, 0) # Top of right wall
    # canvas.create_line(TR - (TR - BR)/3, L - (L - DB)/3, TR, L) # Bottom of right wall
    # canvas.create_line(TR - (TR - BR)/3, DT/3, TR - (TR - BR)/3, L - (L - DB)/3) # Front Corner of right wall
    # canvas.create_line(BR + (TR - BR)/3, DB + (L - DB)/3, BR + (TR - BR)/3, DT - DT/3) # Back corner of right wall
    # canvas.create_line(TL, L, BL, DB) # Bottom of left side
    # canvas.create_line(TL, 0, BL, DT) # Top of left side
    # canvas.create_rectangle(BL, DT, BR, DB) # Doorway
    # canvas.create_line(BR + (TR - BR)/3, DB + (L - DB)/3, BR, DB) # Back Bottom of right side
    # canvas.create_line(BR + (TR - BR)/3, DT - DT/3, BR, DT) # Top of right side
    
    # T shaped split:
    
    # canvas.create_line(TL, L, TL + ((BL - TL)/3), L - ((L - DB)/3)) # bottom of left wall
    # canvas.create_line(TL + ((BL - TL)/3), L - ((L - DB)/3), TL + ((BL - TL)/3), DT/3) # corner of left wall
    # canvas.create_line(TL + ((BL - TL)/3), DT/3, TL, 0) # top of left wall
    # canvas.create_line(TR - (TR - BR)/3, DT - (DT/3), TL + (BL - TL)/3, DT - (DT/3)) # top of back wall
    # canvas.create_line(TR - (TR - BR)/3, DB + (L - DB)/3, TL + (BL - TL)/3, DB + ((L - DB)/3)) # bottom of back wall
    # canvas.create_line(TR - (TR - BR)/3, DT/3, TR, 0) # Top of right wall
    # canvas.create_line(TR - (TR - BR)/3, DT/3, TR - (TR - BR)/3, L - (L - DB)/3) # Corner of right wall
    # canvas.create_line(TR - (TR - BR)/3, L - (L - DB)/3, TR, L) # Bottom of right wall
    
    # X shaped split:
    
    # canvas.create_rectangle(BL, DT, BR, DB) # Doorway
    # canvas.create_line(TL, L, TL + ((BL - TL)/3), L - ((L - DB)/3)) # Front Bottom of left wall
    # canvas.create_line(TL + ((BL - TL)/3), DT/3, TL, 0) # Front Top of left wall
    # canvas.create_line(TL + ((BL - TL)/3), L - ((L - DB)/3), TL + ((BL - TL)/3), DT/3) # Front Corner of left wall
    # canvas.create_line(BL - (BL - TL)/3, DT - (DT/3), BL - (BL - TL)/3, DB + (L - DB)/3) # Back Corner of left wall
    # canvas.create_line(BL - (BL - TL)/3, DB + (L - DB)/3, BL, DB) # Back Bottom of left wall
    # canvas.create_line(BL - (BL - TL)/3, DT - (DT/3), BL, DT) # Back Top of left wall
    # canvas.create_line(TL + (BL - TL)/3, DT - (DT/3), BL - (BL - TL)/3, DT - (DT/3)) # Top of Left hallway wall
    # canvas.create_line(TL + (BL - TL)/3, DB + (L - DB)/3, BL - (BL - TL)/3, DB + ((L - DB)/3)) # Bottom of left hallway wall
    # canvas.create_line(TR - (TR - BR)/3, DT - (DT/3), BR + (BL - TL)/3, DT - (DT/3)) # top of back wall
    # canvas.create_line(TR - (TR - BR)/3, DB + (L - DB)/3, BR + (BL - TL)/3, DB + (L - DB)/3) # bottom of back wall
    # canvas.create_line(TR - (TR - BR)/3, DT/3, TR, 0) # Top of right wall
    # canvas.create_line(TR - (TR - BR)/3, L - (L - DB)/3, TR, L) # Bottom of right wall
    # canvas.create_line(TR - (TR - BR)/3, DT/3, TR - (TR - BR)/3, L - (L - DB)/3) # Front Corner of right wall
    # canvas.create_line(BR + (TR - BR)/3, DB + (L - DB)/3, BR + (TR - BR)/3, DT - DT/3) # Back corner of right wall
    # canvas.create_line(BR + (TR - BR)/3, DB + (L - DB)/3, BR, DB) # Back Bottom of right side
    # canvas.create_line(BR + (TR - BR)/3, DT - DT/3, BR, DT) # Top of right side
    
    # Dead ends:
    
    # canvas.create_line(TL, L, TL + TL/4, L - (L - DB)/4, TL + TL/4, DT/4, TL, 0) # Front Left corner
    # canvas.create_line(TR, L, TR - TL/4, L - (L - DB)/4, TR - TL/4, DT/4, TR, 0) # Front Right corner
    # canvas.create_line(TL + TL/4, DB + 2*(L - DB)/5, TL + 3*TL/5, DB + (L - DB)/8, TR - 3*TL/5, DB + (L - DB)/8, TR - TL/4, DB + 2*(L - DB)/5) # Bottom Back of room
    # canvas.create_line(TL + 3*TL/5, DB + (L - DB)/8, TL + 3*TL/5, DT - DT/4, TL + TL/4, DT - 2.75*DT/5) # Back Left corner
    # canvas.create_line(TR - 3*TL/5, DB + (L - DB)/8, TR - 3*TL/5, DT - DT/4, TR - TL/4, DT - 2.75*DT/5) # Back Right corner
    # canvas.create_line(TL + 3*TL/5, DT - DT/4, TR - 3*TL/5, DT - DT/4) # Top of back wall