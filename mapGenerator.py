import time, random, sys, copy, pickle, math, logging
from tkinter import *

#setting the size of the map and the size of the window
#x_map = int(input("How many rooms wide? "))
#y_map = int(input("How many rooms tall? "))
# def makeCanvas(maxX, maxY):
    # x_map = maxX * 60 + 120
    # y_map = maxY * 60 + 120
    # tk = tkin.Tk()
    # tk.title("Game")
    # tk.resizable(0, 0)
    # tk.wm_attributes("-topmost", 1)
    ## each room will be 60 units by 60 units
    # canvas = tkin.Canvas(tk, width=x_map, height=y_map)
    # canvas.pack()
    # tk.update()
    # return tk, canvas

# the room class
class Room(object):
    def __init__(self, top, bottom, left, right, player, monster, x, y):
        # Directions where there are connections
        self.top = top
        self.bottom = bottom
        self.left = left
        self.right = right
        # Player will be it's own class that will be implemented later
        self.player = player
        # treasure will be items found in the room, might be their own class as well
        self.monster = monster
        # the x y cords of the room with top left being 0 and increasing by one to the right or down
        self.x = x
        self.y = y
        self.rn = self.test()

    def test(self):
        #This returns a number so the program can quickly identify the room it is working with
        #It is a series of true/false checks

        if self.top == True:

            if self.bottom == True:

                if self.left == True:

                    if self.right == True:
                        result = 11 # X shaped room

                    else:
                        result = 8 # T shaped room

                else:

                    if self.right == True:
                        result = 7 # T shaped room

                    else:
                        result = 1 # I shaped room

            else:

                if self.left == True:

                    if self.right == True:
                        result = 9 # T shaped room

                    else:
                        result = 4 # L shaped room

                else:

                    if self.right == True:
                        result = 3 # L shaped room

                    else:
                        result = 13 # dead end opening top

        else:

            if self.bottom == True:

                if self.left == True:

                    if self.right == True:
                        result = 10 # T shaped room

                    else:
                        result = 6 # L shaped room

                else:

                    if self.right == True:
                        result = 5 # L shaped room

                    else:
                        result = 12 # dead end opening bottom

            else:

                if self.left == True:

                    if self.right == True:
                        result = 2 # I shaped room

                    else:
                        result = 14 # dead end opening left

                else:

                    if self.right == True:
                        result = 15 # dead end opening right

                    else:
                        result = 16 # empty

        return result

    def test_direction(self):
        #This is so there is less code to write later on and just makes a dict saying weather there is a connection in a direction
        result = {}
        if self.top == True:
            result['top'] = True
        else:
            result['top'] = False
        if self.bottom == True:
            result['bottom'] = True
        else:
            result['bottom'] = False
        if self.left == True:
            result['left'] = True
        else:
            result['left'] = False
        if self.right == True:
            result['right'] = True
        else:
            result['right'] = False
        return result

    def test_direction_for_drawing(self, side=None):
        if self.rn == 1 or self.rn == 2:
            # Forward and backward
            result = ["forward", "backward"]
    
        elif self.rn == 11:
            # Forward, Backward, Left, Right
            result = ["forward", "backward", "left", "right"]
        
        elif self.rn >= 12 and self.rn <= 15:
            # Backward
            result = ["backward"]
        
        elif side == "north":
            # 5, 6, 7, 8, 10
            if self.rn == 5:
                result = ["backward", "right"]

            elif self.rn == 6:
                result = ["backward", "left"]
            
            elif self.rn == 7:
                result = ["backward", "right", "forward"]

            elif self.rn == 8:
                result = ["backward", "left", "forward"]

            elif self.rn == 10:
                result = ["backward", "right", "left"]
            
        elif side == "south":
            # 3, 4, 7, 8, 9
            if self.rn == 3:
                result = ["backward", "left"]

            elif self.rn == 4:
                result = ["backward", "right"]

            elif self.rn == 7:
                result = ["backward", "left", "forward"]

            elif self.rn == 8:
                result = ["backward", "right", "forward"]

            elif self.rn == 9:
                result = ["backward", "left", "right"]
    
        elif side == "west":
            # 3, 5, 7, 9, 10
            if self.rn == 3:
                result = ["backward", "right"]

            elif self.rn == 5:
                result = ["backward", "left"]

            elif self.rn == 7:
                result = ["backward", "left", "right"]

            elif self.rn == 9:
                result = ["backward", "right", "forward"]

            elif self.rn == 10:
                result = ["backward", "left", "forward"]
    
        elif side == "east":
            # 4, 6, 8, 9, 10
            if self.rn == 4:
                result = ["backward", "left"]

            elif self.rn == 6:
                result = ["backward", "right"]

            elif self.rn == 8:
                result = ["backward", "left", "right"]

            elif self.rn == 9:
                result = ["backward", "left", "forward"]

            elif self.rn == 10:
                result = ["backward", "right", "forward"]
        
        return result

    def drawRoomInterior(self, canvas, side=None):
        BR = 715
        BL = 605
        DB = 415 
        L = 580
        DT = 250
        TR = 1115
        TL = 205
        canvas.create_line(0, L, 1600, L) # Base Line
        if self.rn == 1 or self.rn == 2: # I shaped rooms
            # Will have 1 state, strait ahead
            canvas.create_line(TL, L, BL, DB) # Bottom of left side
            canvas.create_line(TR, L, BR, DB) # Bottom of right side
            canvas.create_rectangle(BL, DT, BR, DB) # Doorway
            canvas.create_line(TL, 0, BL, DT) # Top of left side
            canvas.create_line(TR, 0, BR, DT) # Top of right side

        elif self.rn == 11: # X shaped room
            # Will have 1 state: forward and left and right
            canvas.create_rectangle(BL, DT, BR, DB) # Doorway
            canvas.create_line(TL, L, TL + ((BL - TL)/3), L - ((L - DB)/3)) # Front Bottom of left wall
            canvas.create_line(TL + ((BL - TL)/3), DT/3, TL, 0) # Front Top of left wall
            canvas.create_line(TL + ((BL - TL)/3), L - ((L - DB)/3), TL + ((BL - TL)/3), DT/3) # Front Corner of left wall
            canvas.create_line(BL - (BL - TL)/3, DT - (DT/3), BL - (BL - TL)/3, DB + (L - DB)/3) # Back Corner of left wall
            canvas.create_line(BL - (BL - TL)/3, DB + (L - DB)/3, BL, DB) # Back Bottom of left wall
            canvas.create_line(BL - (BL - TL)/3, DT - (DT/3), BL, DT) # Back Top of left wall
            canvas.create_line(TL + (BL - TL)/3, DT - (DT/3), BL - (BL - TL)/3, DT - (DT/3)) # Top of Left hallway wall
            canvas.create_line(TL + (BL - TL)/3, DB + (L - DB)/3, BL - (BL - TL)/3, DB + ((L - DB)/3)) # Bottom of left hallway wall
            canvas.create_line(TR - (TR - BR)/3, DT - (DT/3), BR + (BL - TL)/3, DT - (DT/3)) # top of back wall
            canvas.create_line(TR - (TR - BR)/3, DB + (L - DB)/3, BR + (BL - TL)/3, DB + (L - DB)/3) # bottom of back wall
            canvas.create_line(TR - (TR - BR)/3, DT/3, TR, 0) # Top of right wall
            canvas.create_line(TR - (TR - BR)/3, L - (L - DB)/3, TR, L) # Bottom of right wall
            canvas.create_line(TR - (TR - BR)/3, DT/3, TR - (TR - BR)/3, L - (L - DB)/3) # Front Corner of right wall
            canvas.create_line(BR + (TR - BR)/3, DB + (L - DB)/3, BR + (TR - BR)/3, DT - DT/3) # Back corner of right wall
            canvas.create_line(BR + (TR - BR)/3, DB + (L - DB)/3, BR, DB) # Back Bottom of right side
            canvas.create_line(BR + (TR - BR)/3, DT - DT/3, BR, DT) # Top of right side
        
        elif self.rn >= 12 and self.rn <= 15: # Dead ends
            # Will have 1 state: None
            canvas.create_line(TL, L, TL + TL/4, L - (L - DB)/4, TL + TL/4, DT/4, TL, 0) # Front Left corner
            canvas.create_line(TR, L, TR - TL/4, L - (L - DB)/4, TR - TL/4, DT/4, TR, 0) # Front Right corner
            canvas.create_line(TL + TL/4, DB + 2*(L - DB)/5, TL + 3*TL/5, DB + (L - DB)/8, TR - 3*TL/5, DB + (L - DB)/8, TR - TL/4, DB + 2*(L - DB)/5) # Bottom Back of room
            canvas.create_line(TL + 3*TL/5, DB + (L - DB)/8, TL + 3*TL/5, DT - DT/4, TL + TL/4, DT - 2.75*DT/5) # Back Left corner
            canvas.create_line(TR - 3*TL/5, DB + (L - DB)/8, TR - 3*TL/5, DT - DT/4, TR - TL/4, DT - 2.75*DT/5) # Back Right corner
            canvas.create_line(TL + 3*TL/5, DT - DT/4, TR - 3*TL/5, DT - DT/4) # Top of back wall

        elif side == "north":
            if self.rn == 5:
                # draw L to the right
                self.L_right(canvas)
            
            elif self.rn == 6:
                # draw L to the left
                self.L_left(canvas)
            
            elif self.rn == 7:
                # draw T to the right
                self.T_right(canvas)

            elif self.rn == 8:
                # draw T to the left
                self.T_left(canvas)

            elif self.rn == 10:
                # draw T split
                self.T_split(canvas)
        
        elif side == "south":
            if self.rn == 3:
                # draw L to the left
                self.L_left(canvas)

            elif self.rn == 4:
                # draw L to the right
                self.L_right(canvas)

            elif self.rn == 7:
                # draw T to the left
                self.T_left(canvas)

            elif self.rn == 8:
                # draw T to the right
                self.T_right(canvas)

            elif self.rn == 9:
                # draw T split
                self.T_split(canvas)
        
        elif side == "east":
            if self.rn == 4:
                # draw L to the left
                self.L_left(canvas)
            
            elif self.rn == 6:
                # draw L to the right
                self.L_right(canvas)

            elif self.rn == 8:
                # draw T split
                self.T_split(canvas)

            elif self.rn == 9:
                # draw T to the left
                self.T_left(canvas)

            elif self.rn == 10:
                # draw T to the right
                self.T_right(canvas)
        
        elif side == "west":
            if self.rn == 3:
                # draw L to the right
                self.L_right(canvas)
            
            elif self.rn == 5:
                # draw L to the left
                self.L_left(canvas)
            
            elif self.rn == 7:
                # draw T split
                self.T_split(canvas)

            elif self.rn == 9:
                # draw T to the right
                self.T_right(canvas)

            elif self.rn == 10:
                # draw T to the left
                self.T_left(canvas)

    def L_left(self, canvas):
        canvas.create_line(TL, L, TL + ((BL - TL)/3), L - ((L - DB)/3)) # bottom of left wall
        canvas.create_line(TL + ((BL - TL)/3), L - ((L - DB)/3), TL + ((BL - TL)/3), DT/3) # corner of left wall
        canvas.create_line(TL + ((BL - TL)/3), DT/3, TL, 0) # top of left wall
        canvas.create_line(TL + ((BL - TL)/3), DT - (DT/3), BR + (TR - BR)/3, DT - (DT/3)) # top of back wall
        canvas.create_line(TL + ((BL - TL)/3), DB + ((L - DB)/3), BR + (TR - BR)/3, DB + ((L - DB)/3)) # bottom of back wall
        canvas.create_line(BR + (TR - BR)/3, DB + ((L - DB)/3), BR + (TR - BR)/3, DT - (DT/3)) # corner of right wall
        canvas.create_line(BR + (TR - BR)/3, DB + ((L - DB)/3), TR, L) # bottom of right wall
        canvas.create_line(BR + (TR - BR)/3, DT - (DT/3), TR, 0) # top of right wall
    
    def L_right(self, canvas):
        canvas.create_line(TR - (TR - BR)/3, DT - (DT/3), BL - (BL - TL)/3, DT - (DT/3)) # top of back wall
        canvas.create_line(TR - (TR - BR)/3, DB + ((L - DB)/3), BL - (BL - TL)/3, DB + ((L - DB)/3)) # bottom of back wall
        canvas.create_line(TR - (TR - BR)/3, DT/3, TR, 0) # Top of right wall
        canvas.create_line(TR - (TR - BR)/3, DT/3, TR - (TR - BR)/3, L - (L - DB)/3) # Corner of right wall
        canvas.create_line(TR - (TR - BR)/3, L - (L - DB)/3, TR, L) # Bottom of right wall
        canvas.create_line(BL - (BL - TL)/3, DT - (DT/3), TL, 0) # Top of left wall
        canvas.create_line(BL - (BL - TL)/3, DT - (DT/3), BL - (BL - TL)/3, DB + (L - DB)/3) # Corner of left wall
        canvas.create_line(BL - (BL - TL)/3, DB + (L - DB)/3, TL, L) # Bottom of left wall

    def T_left(self, canvas):
        canvas.create_line(TL, L, TL + ((BL - TL)/3), L - ((L - DB)/3)) # Front Bottom of left wall
        canvas.create_line(TL + ((BL - TL)/3), DT/3, TL, 0) # Front Top of left wall
        canvas.create_line(TL + ((BL - TL)/3), L - ((L - DB)/3), TL + ((BL - TL)/3), DT/3) # Front Corner of left wall
        canvas.create_line(BL - (BL - TL)/3, DT - (DT/3), BL - (BL - TL)/3, DB + (L - DB)/3) # Back Corner of left wall
        canvas.create_line(BL - (BL - TL)/3, DB + (L - DB)/3, BL, DB) # Back Bottom of left wall
        canvas.create_line(BL - (BL - TL)/3, DT - (DT/3), BL, DT) # Back Top of left wall
        canvas.create_line(TR, L, BR, DB) # Bottom of right wall
        canvas.create_line(TR, 0, BR, DT) # Top of right wall
        canvas.create_rectangle(BL, DT, BR, DB) # Doorway
        canvas.create_line(TL + (BL - TL)/3, DT - (DT/3), BL - (BL - TL)/3, DT - (DT/3)) # Top of Left hallway wall
        canvas.create_line(TL + (BL - TL)/3, DB + (L - DB)/3, BL - (BL - TL)/3, DB + ((L - DB)/3)) # Bottom of left hallway wall

    def T_right(self, canvas):
        canvas.create_line(TR - (TR - BR)/3, DT - (DT/3), BR + (BL - TL)/3, DT - (DT/3)) # top of back wall
        canvas.create_line(TR - (TR - BR)/3, DB + (L - DB)/3, BR + (BL - TL)/3, DB + (L - DB)/3) # bottom of back wall
        canvas.create_line(TR - (TR - BR)/3, DT/3, TR, 0) # Top of right wall
        canvas.create_line(TR - (TR - BR)/3, L - (L - DB)/3, TR, L) # Bottom of right wall
        canvas.create_line(TR - (TR - BR)/3, DT/3, TR - (TR - BR)/3, L - (L - DB)/3) # Front Corner of right wall
        canvas.create_line(BR + (TR - BR)/3, DB + (L - DB)/3, BR + (TR - BR)/3, DT - DT/3) # Back corner of right wall
        canvas.create_line(TL, L, BL, DB) # Bottom of left side
        canvas.create_line(TL, 0, BL, DT) # Top of left side
        canvas.create_rectangle(BL, DT, BR, DB) # Doorway
        canvas.create_line(BR + (TR - BR)/3, DB + (L - DB)/3, BR, DB) # Back Bottom of right side
        canvas.create_line(BR + (TR - BR)/3, DT - DT/3, BR, DT) # Top of right side

    def T_split(self, canvas):
        canvas.create_line(TL, L, TL + ((BL - TL)/3), L - ((L - DB)/3)) # bottom of left wall
        canvas.create_line(TL + ((BL - TL)/3), L - ((L - DB)/3), TL + ((BL - TL)/3), DT/3) # corner of left wall
        canvas.create_line(TL + ((BL - TL)/3), DT/3, TL, 0) # top of left wall
        canvas.create_line(TR - (TR - BR)/3, DT - (DT/3), TL + (BL - TL)/3, DT - (DT/3)) # top of back wall
        canvas.create_line(TR - (TR - BR)/3, DB + (L - DB)/3, TL + (BL - TL)/3, DB + ((L - DB)/3)) # bottom of back wall
        canvas.create_line(TR - (TR - BR)/3, DT/3, TR, 0) # Top of right wall
        canvas.create_line(TR - (TR - BR)/3, DT/3, TR - (TR - BR)/3, L - (L - DB)/3) # Corner of right wall
        canvas.create_line(TR - (TR - BR)/3, L - (L - DB)/3, TR, L) # Bottom of right wall
    
    def drawMonster(self, canvas, side=None, x=None, y=None):
        if x:
            x = x * 60
        else:
            x = self.x * 60
        
        if y:
            y = y * 60
        else:
            y = self.y * 60
        
        x0 = x + 20
        x1 = x + 40
        y0 = y + 20
        y1 = y + 40

        canvas.create_oval(x0, y0, x1, y1)

    def drawPlayer(self, canvas, side="north", x=None, y=None):
        if x:
            x = x * 60
        else:
            x = self.x * 60
        if y:
            y = y * 60
        else:
            y = self.y * 60
        if side == "north":
            x0 = x + 30
            x1 = x + 35
            x2 = x + 30
            x3 = x + 25
            y0 = y + 20
            y1 = y + 35
            y2 = y + 30
            y3 = y + 35
        
        elif side == "south":
            x0 = x + 30
            x1 = x + 35
            x2 = x + 30
            x3 = x + 25
            y0 = y + 40
            y1 = y + 25
            y2 = y + 30
            y3 = y + 25

        elif side == "west":
            x0 = x + 20
            x1 = x + 35
            x2 = x + 30
            x3 = x + 35
            y0 = y + 30
            y1 = y + 35
            y2 = y + 30
            y3 = y + 25

        elif side == "east":
            x0 = x + 40
            x1 = x + 25
            x2 = x + 30
            x3 = x + 25
            y0 = y + 30 
            y1 = y + 35
            y2 = y + 30
            y3 = y + 25
        
        else:
            x0 = x + 30
            x1 = x + 35
            x2 = x + 30
            x3 = x + 25
            y0 = y + 20
            y1 = y + 35
            y2 = y + 30
            y3 = y + 35
        canvas.create_polygon(x0, y0, x1, y1, x2, y2, x3, y3, tags="player")

    def drawRoomExterior(self, canvas, side=None, x=None, y=None, face=None):
        logging.debug("started drawRoomExterior")
        logging.debug("rn is %s" % (self.rn))
        # this draws the room and returns the image so it can be deleted later on if need be
        if x:
            x1 = x * 60
        else:
            x1 = self.x * 60
        if y:
            y1 = y * 60
        else:
            y1 = self.y * 60
        
        def NS(x1, y1): # rn = 1
            x1 += 10
            x2 = x1 + 40
            y2 = y1 + 60
            a = canvas.create_line(x1, y1, x1, y2)
            b = canvas.create_line(x2, y1, x2, y2)
            shape = [a, b]
            return shape
        
        def EW(x1, y1): # rn = 2
            x2 = x1 + 60
            y1 += 10
            y2 = y1 + 40
            a = canvas.create_line(x1, y1, x2, y1)
            b = canvas.create_line(x1, y2, x2, y2)
            shape = [a, b]
            return shape
        
        def NE(x1, y1): # rn = 3
            x1 += 10
            x2 = x1 + 40
            y2 = y1 + 10
            x3 = x2 + 10
            y3 = y2 + 40
            a = canvas.create_line(x1, y1, x1, y3)
            b = canvas.create_line(x1, y3, x3, y3)
            c = canvas.create_line(x2, y1, x2, y2)
            d = canvas.create_line(x2, y2, x3, y2)
            shape = [a, b, c, d]
            return shape

        def NW(x1, y1): # rn = 4
            x2 = x1 + 10
            x3 = x2 + 40
            y2 = y1 + 10
            y3 = y2 + 40
            a = canvas.create_line(x1, y2, x2, y2)
            b = canvas.create_line(x2, y1, x2, y2)
            c = canvas.create_line(x1, y3, x3, y3)
            d = canvas.create_line(x3, y3, x3, y1)
            shape = [a, b, c, d]
            return shape

        def SE(x1, y1): # rn = 5
            x1 += 10
            x2 = x1 + 40
            x3 = x2 + 10
            y1 += 10
            y2 = y1 + 40
            y3 = y2 + 10
            a = canvas.create_line(x1, y1, x1, y3)
            b = canvas.create_line(x1, y1, x3, y1)
            c = canvas.create_line(x2, y2, x3, y2)
            d = canvas.create_line(x2, y2, x2, y3)
            shape = [a, b, c, d]
            return shape
        
        def SW(x1, y1): # rn = 6
            x2 = x1 + 10
            x3 = x2 + 40
            y1 += 10
            y2 = y1 + 40
            y3 = y2 + 10
            a = canvas.create_line(x1, y1, x3, y1)
            b = canvas.create_line(x3, y1, x3, y3)
            c = canvas.create_line(x1, y2, x2, y2)
            d = canvas.create_line(x2, y2, x2, y3)
            shape = [a, b, c, d]
            return shape
        
        def NSE(x1, y1): # rn = 7
            x1 += 10
            x2 = x1 + 40
            x3 = x2 + 10
            y2 = y1 + 10
            y3 = y2 + 40
            y4 = y3 + 10
            a = canvas.create_line(x1, y1, x1, y4)
            b = canvas.create_line(x2, y1, x2, y2)
            c = canvas.create_line(x2, y2, x3, y2)
            d = canvas.create_line(x2, y3, x3, y3)
            e = canvas.create_line(x2, y3, x2, y4)
            shape = [a, b, c, d, e]
            return shape
        
        def NSW(x1, y1): # rn = 8
            x2 = x1 + 10
            x3 = x2 + 40
            y2 = y1 + 10
            y3 = y2 + 40
            y4 = y3 + 10
            a = canvas.create_line(x3, y1, x3, y4)
            b = canvas.create_line(x2, y1, x2, y2)
            c = canvas.create_line(x1, y2, x2, y2)
            d = canvas.create_line(x1, y3, x2, y3)
            e = canvas.create_line(x2, y3, x2, y4)
            shape = [a, b, c, d, e]
            return shape

        def NEW(x1, y1): # rn = 9
            x2 = x1 + 10
            x3 = x2 + 40
            x4 = x3 + 10
            y2 = y1 + 10
            y3 = y2 + 40
            a = canvas.create_line(x1, y3, x4, y3)
            b = canvas.create_line(x1, y2, x2, y2)
            c = canvas.create_line(x2, y1, x2, y2)
            d = canvas.create_line(x3, y1, x3, y2)
            e = canvas.create_line(x3, y2, x4, y2)
            shape = [a, b, c, d, e]
            return shape
        
        def SEW(x1, y1): # rn = 10
            x2 = x1 + 10
            x3 = x2 + 40
            x4 = x3 + 10
            y1 += 10
            y2 = y1 + 40
            y3 = y2 + 10
            a = canvas.create_line(x1, y1, x4, y1)
            b = canvas.create_line(x1, y2, x2, y2)
            c = canvas.create_line(x2, y2, x2, y3)
            d = canvas.create_line(x3, y2, x3, y3)
            e = canvas.create_line(x3, y2, x4, y2)
            shape = [a, b, c, d, e]
            return shape

        def NSEW(x1, y1): # rn = 11
            x2 = x1 + 10
            x3 = x2 + 40
            x4 = x3 + 10
            y2 = y1 + 10
            y3 = y2 + 40
            y4 = y3 + 10
            a = canvas.create_line(x2, y1, x2, y2)
            b = canvas.create_line(x3, y1, x3, y2)
            c = canvas.create_line(x1, y2, x2, y2)
            d = canvas.create_line(x3, y2, x4, y2)
            e = canvas.create_line(x1, y3, x2, y3)
            f = canvas.create_line(x3, y3, x4, y3)
            g = canvas.create_line(x2, y3, x2, y4)
            h = canvas.create_line(x3, y3, x3, y4)
            shape = [a, b, c, d, e, f, g, h]
            return shape

        def _S(x1, y1): # rn = 12
            x1 += 5
            x2 = x1 + 5
            x3 = x2 + 40
            x4 = x3 + 5
            y1 += 5
            y2 = y1 + 50
            y3 = y2 + 10
            a = canvas.create_line(x1, y1, x4, y1)
            b = canvas.create_line(x1, y1, x1, y2)
            c = canvas.create_line(x4, y1, x4, y2)
            d = canvas.create_line(x1, y2, x2, y2)
            e = canvas.create_line(x2, y2, x2, y3)
            f = canvas.create_line(x3, y2, x4, y2)
            g = canvas.create_line(x3, y2, x3, y3)
            shape = [a, b, c, d, e, f, g]
            return shape
        
        def _N(x1, y1): # rn = 13
            x1 += 5
            x2 = x1 + 5
            x3 = x2 + 40
            x4 = x3 + 5
            y2 = y1 + 5
            y3 = y2 + 50
            a = canvas.create_line(x1, y3, x4, y3)
            b = canvas.create_line(x1, y2, x1, y3)
            c = canvas.create_line(x4, y2, x4, y3)
            d = canvas.create_line(x1, y2, x2, y2)
            e = canvas.create_line(x2, y1, x2, y2)
            f = canvas.create_line(x3, y1, x3, y2)
            g = canvas.create_line(x3, y2, x4, y2)
            shape = [a, b, c, d, e, f, g]
            return shape
        
        def _W(x1, y1): # rn = 14
            x2 = x1 + 5
            x3 = x2 + 50
            y1 += 5
            y2 = y1 + 5
            y3 = y2 + 40
            y4 = y3 + 5
            a = canvas.create_line(x1, y2, x2, y2)
            b = canvas.create_line(x2, y2, x2, y1)
            c = canvas.create_line(x2, y1, x3, y1)
            d = canvas.create_line(x3, y1, x3, y4)
            e = canvas.create_line(x3, y4, x2, y4)
            f = canvas.create_line(x2, y4, x2, y3)
            g = canvas.create_line(x2, y3, x1, y3)
            shape = [a, b, c, d, e, f, g]
            return shape
        
        def _E(x1, y1): # rn = 15
            x1 += 5
            x2 = x1 + 50
            x3 = x2 + 10
            y1 += 5
            y2 = y1 + 5
            y3 = y2 + 40
            y4 = y3 + 5
            a = canvas.create_line(x3, y2, x2, y2)
            b = canvas.create_line(x2, y2, x2, y1)
            c = canvas.create_line(x2, y1, x1, y1)
            d = canvas.create_line(x1, y1, x1, y4)
            e = canvas.create_line(x1, y4, x2, y4)
            f = canvas.create_line(x2, y4, x2, y3)
            g = canvas.create_line(x2, y3, x3, y3)
            shape = [a, b, c, d, e, f, g]
            return shape
        
        def Empty(x1, y1): # rn = 16/None
            x2 = x1 + 60
            y2 = y1 + 60
            a = canvas.create_rectangle(x1, y1, x2, y2, fill='white', outline='white')
            shape = [a]
            return shape

        if face:
            # draw the room facing a certain direction 
            if self.player == True:
                logging.debug("drew player at %s, %s facing %s" % (self.x, self.y, "north"))
                self.drawPlayer(canvas, "north", x=x, y=y)
            
            if self.monster == True:
                self.drawMonster(canvas, side, x=x, y=y)

            if face == "north":
                
                if self.rn == 1:
                    shape = NS(x1, y1)
                
                elif self.rn == 2:
                    shape = EW(x1, y1)

                elif self.rn == 3:
                    shape = NE(x1, y1)
                
                elif self.rn == 4:
                    shape = NW(x1, y1)

                elif self.rn == 5:
                    shape = SE(x1, y1)
                
                elif self.rn == 6:
                    shape = SW(x1, y1)
                
                elif self.rn == 7:
                    shape = NSE(x1, y1)

                elif self.rn == 8:
                    shape = NSW(x1, y1)

                elif self.rn == 9:
                    shape = NEW(x1, y1)

                elif self.rn == 10:
                    shape = SEW(x1, y1)

                elif self.rn == 11:
                    shape = NSEW(x1, y1)

                elif self.rn == 12:
                    shape = _S(x1, y1)
                
                elif self.rn == 13:
                    shape = _N(x1, y1)

                elif self.rn == 14:
                    shape = _W(x1, y1)

                elif self.rn == 15:
                    shape = _E(x1, y1)
                
                else:
                    shape = Empty(x1, y1)
            
            elif face == "east":

                if self.rn == 1:
                    shape = EW(x1, y1)

                elif self.rn == 2:
                    shape = NS(x1, y1)
                
                elif self.rn == 3:
                    shape = NW(x1, y1)
                
                elif self.rn == 4:
                    shape = SW(x1, y1)
                
                elif self.rn == 5:
                    shape = NE(x1, y1)

                elif self.rn == 6:
                    shape = SE(x1, y1)
                
                elif self.rn == 7:
                    shape = NEW(x1, y1)
                
                elif self.rn == 8:
                    shape = SEW(x1, y1)

                elif self.rn == 9:
                    shape = NSW(x1, y1)
                
                elif self.rn == 10:
                    shape = NSE(x1, y1)

                elif self.rn == 11:
                    shape = NSEW(x1, y1)

                elif self.rn == 12:
                    shape = _E(x1, y1)

                elif self.rn == 13:
                    shape = _W(x1, y1)

                elif self.rn == 14:
                    shape = _S(x1, y1)
                
                elif self.rn == 15:
                    shape = _N(x1, y1)

                else:
                    shape = Empty(x1, y1)

            elif face == "south":

                if self.rn == 1:
                    shape = NS(x1, y1)

                elif self.rn == 2:
                    shape = EW(x1, y1)

                elif self.rn == 3:
                    shape = SW(x1, y1)
                
                elif self.rn == 4:
                    shape = SE(x1, y1)

                elif self.rn == 5:
                    shape = NW(x1, y1)
                
                elif self.rn == 6:
                    shape = NE(x1, y1)
                
                elif self.rn == 7:
                    shape = NSW(x1, y1)
                
                elif self.rn == 8:
                    shape = NSE(x1, y1)

                elif self.rn == 9:
                    shape = SEW(x1, y1)

                elif self.rn == 10:
                    shape = NEW(x1, y1)
                
                elif self.rn == 11:
                    shape = NSEW(x1, y1)

                elif self.rn == 12:
                    shape = _N(x1, y1)

                elif self.rn == 13:
                    shape = _S(x1, y1)

                elif self.rn == 14:
                    shape = _E(x1, y1)

                elif self.rn == 15:
                    shape = _W(x1, y1)
                
                else:
                    shape = Empty(x1, y1)

            elif face == "west":

                if self.rn == 1: 
                    shape = EW(x1, y1)

                elif self.rn == 2:
                    shape = NS(x1, y1)

                elif self.rn == 3:
                    shape = SE(x1, y1)
                
                elif self.rn == 4:
                    shape = NE(x1, y1)

                elif self.rn == 5:
                    shape = SW(x1, y1)

                elif self.rn == 6:
                    shape = NW(x1, y1)

                elif self.rn == 7:
                    shape = SEW(x1, y1)

                elif self.rn == 8:
                    shape = NEW(x1, y1)

                elif self.rn == 9:
                    shape = NSE(x1, y1)

                elif self.rn == 10:
                    shape = NSW(x1, y1)

                elif self.rn == 11:
                    shape = NSEW(x1, y1)

                elif self.rn == 12:
                    shape = _E(x1, y1)

                elif self.rn == 13:
                    shape = _W(x1, y1)
                
                elif self.rn == 14:
                    shape = _N(x1, y1)

                elif self.rn == 15:
                    shape = _S(x1, y1)

                else:
                    shape = Empty(x1, y1)
        else: 
            # just do normal draw
            if self.player == True:
                logging.debug("drew player at %s, %s facing %s" % (self.x, self.y, side))
                self.drawPlayer(canvas, side, x=x, y=y)
            
            if self.monster == True:
                self.drawMonster(canvas, side, x=x, y=y)
            
            if self.rn == 1:
                x1 += 10
                x2 = x1 + 40
                y2 = y1 + 60
                a = canvas.create_line(x1, y1, x1, y2)
                b = canvas.create_line(x2, y1, x2, y2)
                shape = [a, b]
            elif self.rn == 2:
                x2 = x1 + 60
                y1 += 10
                y2 = y1 + 40
                a = canvas.create_line(x1, y1, x2, y1)
                b = canvas.create_line(x1, y2, x2, y2)
                shape = [a, b]
            elif self.rn == 3:
                x1 += 10
                x2 = x1 + 40
                y2 = y1 + 10
                x3 = x2 + 10
                y3 = y2 + 40
                a = canvas.create_line(x1, y1, x1, y3)
                b = canvas.create_line(x1, y3, x3, y3)
                c = canvas.create_line(x2, y1, x2, y2)
                d = canvas.create_line(x2, y2, x3, y2)
                shape = [a, b, c, d]
            elif self.rn == 4:
                x2 = x1 + 10
                x3 = x2 + 40
                y2 = y1 + 10
                y3 = y2 + 40
                a = canvas.create_line(x1, y2, x2, y2)
                b = canvas.create_line(x2, y1, x2, y2)
                c = canvas.create_line(x1, y3, x3, y3)
                d = canvas.create_line(x3, y3, x3, y1)
                shape = [a, b, c, d]
            elif self.rn == 5:
                x1 += 10
                x2 = x1 + 40
                x3 = x2 + 10
                y1 += 10
                y2 = y1 + 40
                y3 = y2 + 10
                a = canvas.create_line(x1, y1, x1, y3)
                b = canvas.create_line(x1, y1, x3, y1)
                c = canvas.create_line(x2, y2, x3, y2)
                d = canvas.create_line(x2, y2, x2, y3)
                shape = [a, b, c, d]
            elif self.rn == 6:
                x2 = x1 + 10
                x3 = x2 + 40
                y1 += 10
                y2 = y1 + 40
                y3 = y2 + 10
                a = canvas.create_line(x1, y1, x3, y1)
                b = canvas.create_line(x3, y1, x3, y3)
                c = canvas.create_line(x1, y2, x2, y2)
                d = canvas.create_line(x2, y2, x2, y3)
                shape = [a, b, c, d]
            elif self.rn == 7:
                x1 += 10
                x2 = x1 + 40
                x3 = x2 + 10
                y2 = y1 + 10
                y3 = y2 + 40
                y4 = y3 + 10
                a = canvas.create_line(x1, y1, x1, y4)
                b = canvas.create_line(x2, y1, x2, y2)
                c = canvas.create_line(x2, y2, x3, y2)
                d = canvas.create_line(x2, y3, x3, y3)
                e = canvas.create_line(x2, y3, x2, y4)
                shape = [a, b, c, d, e]
            elif self.rn == 8:
                x2 = x1 + 10
                x3 = x2 + 40
                y2 = y1 + 10
                y3 = y2 + 40
                y4 = y3 + 10
                a = canvas.create_line(x3, y1, x3, y4)
                b = canvas.create_line(x2, y1, x2, y2)
                c = canvas.create_line(x1, y2, x2, y2)
                d = canvas.create_line(x1, y3, x2, y3)
                e = canvas.create_line(x2, y3, x2, y4)
                shape = [a, b, c, d, e]
            elif self.rn == 9:
                x2 = x1 + 10
                x3 = x2 + 40
                x4 = x3 + 10
                y2 = y1 + 10
                y3 = y2 + 40
                a = canvas.create_line(x1, y3, x4, y3)
                b = canvas.create_line(x1, y2, x2, y2)
                c = canvas.create_line(x2, y1, x2, y2)
                d = canvas.create_line(x3, y1, x3, y2)
                e = canvas.create_line(x3, y2, x4, y2)
                shape = [a, b, c, d, e]
            elif self.rn == 10:
                x2 = x1 + 10
                x3 = x2 + 40
                x4 = x3 + 10
                y1 += 10
                y2 = y1 + 40
                y3 = y2 + 10
                a = canvas.create_line(x1, y1, x4, y1)
                b = canvas.create_line(x1, y2, x2, y2)
                c = canvas.create_line(x2, y2, x2, y3)
                d = canvas.create_line(x3, y2, x3, y3)
                e = canvas.create_line(x3, y2, x4, y2)
                shape = [a, b, c, d, e]
            elif self.rn == 11:
                x2 = x1 + 10
                x3 = x2 + 40
                x4 = x3 + 10
                y2 = y1 + 10
                y3 = y2 + 40
                y4 = y3 + 10
                a = canvas.create_line(x2, y1, x2, y2)
                b = canvas.create_line(x3, y1, x3, y2)
                c = canvas.create_line(x1, y2, x2, y2)
                d = canvas.create_line(x3, y2, x4, y2)
                e = canvas.create_line(x1, y3, x2, y3)
                f = canvas.create_line(x3, y3, x4, y3)
                g = canvas.create_line(x2, y3, x2, y4)
                h = canvas.create_line(x3, y3, x3, y4)
                shape = [a, b, c, d, e, f, g, h]
            elif self.rn == 12:
                x1 += 5
                x2 = x1 + 5
                x3 = x2 + 40
                x4 = x3 + 5
                y1 += 5
                y2 = y1 + 50
                y3 = y2 + 10
                a = canvas.create_line(x1, y1, x4, y1)
                b = canvas.create_line(x1, y1, x1, y2)
                c = canvas.create_line(x4, y1, x4, y2)
                d = canvas.create_line(x1, y2, x2, y2)
                e = canvas.create_line(x2, y2, x2, y3)
                f = canvas.create_line(x3, y2, x4, y2)
                g = canvas.create_line(x3, y2, x3, y3)
                shape = [a, b, c, d, e, f, g]
            elif self.rn == 13:
                x1 += 5
                x2 = x1 + 5
                x3 = x2 + 40
                x4 = x3 + 5
                y2 = y1 + 5
                y3 = y2 + 50
                a = canvas.create_line(x1, y3, x4, y3)
                b = canvas.create_line(x1, y2, x1, y3)
                c = canvas.create_line(x4, y2, x4, y3)
                d = canvas.create_line(x1, y2, x2, y2)
                e = canvas.create_line(x2, y1, x2, y2)
                f = canvas.create_line(x3, y1, x3, y2)
                g = canvas.create_line(x3, y2, x4, y2)
                shape = [a, b, c, d, e, f, g]
            elif self.rn == 14:
                x2 = x1 + 5
                x3 = x2 + 50
                y1 += 5
                y2 = y1 + 5
                y3 = y2 + 40
                y4 = y3 + 5
                a = canvas.create_line(x1, y2, x2, y2)
                b = canvas.create_line(x2, y2, x2, y1)
                c = canvas.create_line(x2, y1, x3, y1)
                d = canvas.create_line(x3, y1, x3, y4)
                e = canvas.create_line(x3, y4, x2, y4)
                f = canvas.create_line(x2, y4, x2, y3)
                g = canvas.create_line(x2, y3, x1, y3)
                shape = [a, b, c, d, e, f, g]
            elif self.rn == 15:
                x1 += 5
                x2 = x1 + 50
                x3 = x2 + 10
                y1 += 5
                y2 = y1 + 5
                y3 = y2 + 40
                y4 = y3 + 5
                a = canvas.create_line(x3, y2, x2, y2)
                b = canvas.create_line(x2, y2, x2, y1)
                c = canvas.create_line(x2, y1, x1, y1)
                d = canvas.create_line(x1, y1, x1, y4)
                e = canvas.create_line(x1, y4, x2, y4)
                f = canvas.create_line(x2, y4, x2, y3)
                g = canvas.create_line(x2, y3, x3, y3)
                shape = [a, b, c, d, e, f, g]
            else:
                x2 = x1 + 60
                y2 = y1 + 60
                a = canvas.create_rectangle(x1, y1, x2, y2, fill='white', outline='white')
                shape = [a]
        return shape
    
#Basic rooms to build the framework of the map
frame_room = Room(False, False, False, False, False, False, 0, 0)
rooms = Room(True, True, True, True, False, False, 0, 0)
class MapForGame(object):
    def __init__(self, maxX=19, maxY=11):
        self.knownRooms = []
        self.gameMap = self.finishMap(maxX, maxY)
        

    #builds the framework of the map
    def makeArray(self, max_x, max_y):
        #the empty frame
        array = []
        
        #Makes sub-lists for each room in y

        for y in range(max_y + 2):
            array.append([frame_room])
        #adds x amount of rooms to the width of the map and puts a frame on the end

        for y in range(1, max_y + 1):

            for x in range(1, max_x + 1):
                array[y].append(rooms)
            array[y].append(frame_room)
        #adds a frame to the top and the bottom of the framework

        for x in range(max_x + 1):
            array[0].append(frame_room)
            array[max_y + 1].append(frame_room)

        return array
    
    def makeMap(self, max_x, max_y):
        #the algorithem that builds the map
        # z = 0.67 #the chance for a connection to be made to the bottom or the right of any room
        z = float(random.randint(35, 75) / 100)
        map_frame = self.makeArray(max_x, max_y) #creats a framework for the map to be put in
        
        #runs once for every entry in the map frame

        for y in range(len(map_frame)):

            for x in range(len(map_frame[y])):

                if map_frame[y][x] != frame_room: #checks to make sure that it is not creating a room on the border of the map
                #similar to the room test in exicution in that it is a serise of true - false statements

                    if map_frame[y - 1][x].bottom == False:

                        if map_frame[y][x - 1].right == False:

                            if map_frame[y][x + 1].left == False:

                                if map_frame[y + 1][x].top == False:
                                    map_frame[y][x] = Room(False, False, False, False, False, False, x, y)

                                else:
                                    ynb = 0.01 * float(random.randint(1, 100)) #random chance is done like this so I can change the sensitivity if I want, say I want 67.5% chance

                                    if ynb <= z:
                                        map_frame[y][x] = Room(False, True, False, False, False, False, x, y)

                                    else:
                                        map_frame[y][x] = Room(False, False, False, False, False, False, x, y)

                            else:
                                ynr = 0.01 * float(random.randint(1, 100))

                                if map_frame[y + 1][x].top == False:

                                    if ynr <= z:
                                        map_frame[y][x] = Room(False, False, False, True, False, False, x, y)
                                   
                                    else:
                                        map_frame[y][x] = Room(False, False, False, False, False, False, x, y)
             
                                else:
                                    ynd = 0.01 * float(random.randint(1, 100))

                                    if ynr <= z:

                                        if ynd <= z:
                                            map_frame[y][x] = Room(False, True, False, True, False, False, x, y)

                                        else:
                                            map_frame[y][x] = Room(False, False, False, True, False, False, x, y)

                                    else:

                                        if ynd <= z:
                                            map_frame[y][x] = Room(False, True, False, False, False, False, x, y)

                                        else:
                                            map_frame[y][x] = Room(False, False, False, False, False, False, x, y)

                        else:

                            if map_frame[y][x + 1].left == False:

                                if map_frame[y + 1][x].top == False:
                                    map_frame[y][x] = Room(False, False, True, False, False, False, x, y)

                                else:
                                    ynb = 0.01 * float(random.randint(1, 100))

                                    if ynb <= z:
                                        map_frame[y][x] = Room(False, True, True, False, False, False, x, y)

                                    else:
                                         map_frame[y][x] = Room(False, False, True, False, False, False, x, y)

                            else:
                                ynr = 0.01 * float(random.randint(1, 100))

                                if map_frame[y + 1][x].top == False:

                                    if ynr <= z:
                                        map_frame[y][x] = Room(False, False, True, True, False, False, x, y)

                                    else:
                                        map_frame[y][x] = Room(False, False, True, False, False, False, x, y)

                                else:
                                    ynd = 0.01 * float(random.randint(1, 100))

                                    if ynr <= z:

                                        if ynd <= z:
                                            map_frame[y][x] = Room(False, True, True, True, False, False, x, y)

                                        else:
                                            map_frame[y][x] = Room(False, False, True, True, False, False, x, y)

                                    else:

                                        if ynd <= z:
                                            map_frame[y][x] = Room(False, True, True, False, False, False, x, y)

                                        else:
                                            map_frame[y][x] = Room(False, False, True, False, False, False, x, y)

                    else:

                        if map_frame[y][x - 1].right == False:

                            if map_frame[y][x + 1].left == False:

                                if map_frame[y + 1][x].top == False:
                                    map_frame[y][x] = Room(True, False, False, False, False, False, x, y)

                                else:
                                    ynb = 0.01 * float(random.randint(1, 100))

                                    if ynb <= z:
                                        map_frame[y][x] = Room(True, True, False, False, False, False, x, y)

                                    else:
                                        map_frame[y][x] = Room(True, False, False, False, False, False, x, y)

                            else:
                                ynr = 0.01 * float(random.randint(1, 100))

                                if map_frame[y + 1][x].top == False:

                                    if ynr <= z:
                                        map_frame[y][x] = Room(True, False, False, True, False, False, x, y)

                                    else:
                                        map_frame[y][x] = Room(True, False, False, False, False, False, x, y)

                                else:
                                    ynd = 0.01 * float(random.randint(1, 100))

                                    if ynr <= z:

                                        if ynd <= z:
                                            map_frame[y][x] = Room(True, True, False, True, False, False, x, y)

                                        else:
                                            map_frame[y][x] = Room(True, False, False, True, False, False, x, y)

                                    else:

                                        if ynd <= z:
                                            map_frame[y][x] = Room(True, True, False, False, False, False, x, y)

                                        else:
                                            map_frame[y][x] = Room(True, False, False, False, False, False, x, y)

                        else:

                            if map_frame[y][x + 1].left == False:

                                if map_frame[y + 1][x].top == False:
                                    map_frame[y][x] = Room(True, False, True, False, False, False, x, y)

                                else:
                                    ynb = 0.01 * float(random.randint(1, 100))

                                    if ynb <= z:
                                        map_frame[y][x] = Room(True, True, True, False, False, False, x, y)

                                    else:
                                        map_frame[y][x] = Room(True, False, True, False, False, False, x, y)

                            else:
                                ynr = 0.01 * float(random.randint(1, 100))

                                if map_frame[y + 1][x] == 1 or map_frame[y + 1][x].top == False:

                                    if ynr <= z:
                                        map_frame[y][x] = Room(True, False, True, True, False, False, x, y)

                                    else:
                                        map_frame[y][x] = Room(True, False, True, False, False, False, x, y)

                                else:
                                    ynd = 0.01 * float(random.randint(1, 100))

                                    if ynr <= z:

                                        if ynd <= z:
                                            map_frame[y][x] = Room(True, True, True, True, False, False, x, y)

                                        else:
                                            map_frame[y][x] = Room(True, False, True, True, False, False, x, y)

                                    else:

                                        if ynd <= z:
                                            map_frame[y][x] = Room(True, True, True, False, False, False, x, y)

                                        else:
                                            map_frame[y][x] = Room(True, False, True, False, False, False, x, y)

        return map_frame

    #draws the map room by room and returns a map of shapes so I could delete a specific room if I know the x y cords
    def drawMap(self, canvas, map_frame):
        canvas.delete(ALL)
        b = len(map_frame) - 2
        a = len(map_frame[0]) - 2
        shapes = self.makeArray(a, b)
    
        for y in range(len(map_frame)):
    
            for x in range(len(map_frame[y])):
                shapes[y][x] = map_frame[y][x].drawRoomExterior(canvas)
    
        return shapes

    def drawMiniMap(self, canvas, map_frame, knownRooms, cords, side=None):
        logging.debug("Started drawMiniMap")
        test = 0
        a = cords[0] - 1
        b = cords[1] - 1
        for Y in range(3):
            for X in range(3):
                for room in knownRooms:
                    if room['y'] == a + Y and room['x'] == b + X:
                        test += 1
                        A = Y
                        B = X
                        logging.debug("Drawing room at %s, %s" % (a + Y, b + X))
                        # South can only move by 2 in one direction or 2 in two directions
                        # East and West only move by 2 points, either 2 in one direction or 1 in two directions
                        # If X and Y are even numbers then East and West move 2 in one and South by 2 in two 
                        # If X or Y are odd numbers then East and West move 1 in two and South by 2 in one
                        # For East or counter-clockwise X is equal to the number of negitive points
                        # For East or counter-clockwise if Y is 0 then all positive points are in A (and all negitive are in B) and if Y is 2 then all postitive points are in B (and all negitive are in A)
                        # For West or clockwise Y is equal to the number of negitive points
                        # For West or clockwise if X is 0 then all positive points are in B and if X is 2 then all positive points are in A
                        # For South if X or Y are equal to 2 then the number of positions that are is equal to the number of negiitive movements 
                        # For South if X is 0 then B is 2, if X is 1 then B is 0, if X is 2 then B is -2, the same is true for Y and A

                        if side == 'east': # 90 degrees counter-clockwise
                            A += (2 - Y - X)
                            B += (Y - X)
                            # if Y == 0:
                            #     A += (2 - X)
                            #     B -= X
                            # elif Y == 1:
                            #     A += (Y - X)
                            #     B += (Y - X)
                            # else:
                            #     A -= X
                            #     B += (2 - X)
                            
                        elif side == 'west': # 90 degrees clockwise
                            A += (X - Y)
                            B += (2 - X - Y)
                            # if X == 0:
                            #     A -= Y
                            #     B += (2 - Y)
                            # elif X == 1:
                            #     A += (X - Y)
                            #     B += (X - Y)
                            # else:
                            #     A += (2 - Y)
                            #     B -= Y

                        elif side == 'south': # 180 degrees
                            A += 2*(1 - Y)
                            B += 2*(1 - X)
                                
                        # if X == 0 and Y == 0: 
                        #     if side == "east": # 90 degrees left or counter-clockwise
                        #         B += 0
                        #         A += 2
                            
                        #     elif side == "south": # 180 degrees
                        #         B += 2
                        #         A += 2
                            
                        #     elif side == "west": # 90 degrees right or clockwise
                        #         B += 2
                        #         A += 0

                        # elif X == 0 and Y == 1:
                        #     if side == "east": # 90 degrees left or counter-clockwise
                        #         B += 1
                        #         A += 1

                        #     elif side == "south": # 180 degrees
                        #         B += 2
                        #         A += 0
                            
                        #     elif side == "west": # 90 degrees right or clockwise
                        #         B += 1
                        #         A -= 1
                        
                        # elif X == 0 and Y == 2: 
                        #     if side == "east": # 90 degrees left or counter-clockwise
                        #         B += 2
                        #         A += 0

                        #     elif side == "south": # 180 degrees
                        #         B += 2
                        #         A -= 2
                            
                        #     elif side == "west": # 90 degrees right or clockwise
                        #         B += 0
                        #         A -= 2

                        # elif X == 1 and Y == 0: 
                        #     if side == "east": # 90 degrees left or counter-clockwise
                        #         B -= 1
                        #         A += 1

                        #     elif side == "south": # 180 degrees
                        #         B += 0
                        #         A += 2
                            
                        #     elif side == "west": # 90 degrees right or clockwise
                        #         B += 1
                        #         A += 1

                        # elif X == 1 and Y == 2: 
                        #     if side == "east": # 90 degrees left or counter-clockwise
                        #         B += 1
                        #         A -= 1
                            
                        #     elif side == "south": # 180 degrees
                        #         B += 0
                        #         A -= 2
                            
                        #     elif side == "west": # 90 degrees right or clockwise
                        #         B -= 1
                        #         A -= 1
                        
                        # elif X == 2 and Y == 0: 
                        #     if side == "east": # 90 degrees left or counter-clockwise
                        #         B -= 2
                        #         A += 0

                        #     elif side == "south": # 180 degrees
                        #         B -= 2
                        #         A += 2
                            
                        #     elif side == "west": # 90 degrees right or clockwise
                        #         B += 0
                        #         A += 2

                        # elif X == 2 and Y == 1: 
                        #     if side == "east": # 90 degrees left or counter-clockwise
                        #         B -= 1
                        #         A -= 1
                            
                        #     elif side == "south": # 180 degrees
                        #         B -= 2
                        #         A += 0
                            
                        #     elif side == "west": # 90 degrees right or clockwise
                        #         B -= 1
                        #         A += 1
                        
                        # elif X == 2 and Y == 2: 
                        #     if side == "east": # 90 degrees left or counter-clockwise
                        #         B += 0
                        #         A -= 2
                            
                        #     elif side == "south": # 180 degrees
                        #         B -= 2
                        #         A -= 2
                            
                        #     elif side == "west": # 90 degrees right or clockwise
                        #         B -= 2
                        #         A += 0

                        map_frame[a + Y][b + X].drawRoomExterior(canvas, side, x=(19 + B), y=(10 + A), face=side)
        logging.debug("Drew %s rooms for the miniMap" % (test))
        logging.debug("Finished drawMiniMap")

    def drawKnownRooms(self, canvas, map_frame, knownRooms, side="north"):
        logging.debug("started drawKnownRooms with %s rooms discovered" % (len(knownRooms)))
        test = 0
        canvas.delete(ALL)
        b = len(map_frame) - 2
        a = len(map_frame[0]) - 2
        shapes = self.makeArray(a, b)
        for y in range(len(map_frame)):
            for x in range(len(map_frame[y])):
                for room in knownRooms:
                    if room['x'] == x and room['y'] == y:
                        test += 1
                        shapes[y][x] = map_frame[y][x].drawRoomExterior(canvas, side)
        
        logging.debug("finished drawKnownRooms with %s rooms discovered and %s rooms drawn" % (len(knownRooms), test))
        return shapes
    
    #tests the map to see if the size of the room set is large enough (Formula for that is still being tweaked)
    def testMap(self, map_frame):
        frame = copy.deepcopy(map_frame) #makes a deep copy of the map to edit so the program can count the room sets
        set = False
        set_num = 0
        sets = {}
        frame_room = Room(False, False, False, False, False, False, 0, 0)
      
        for y in range(len(frame)):

            for x in range(len(frame[y])):

                if frame[y][x].top == True or frame[y][x].bottom == True or frame[y][x].left == True or frame[y][x].right == True: #checks every room to see if it is a new set or if it is a edge peice/old set
                    a = y
                    b = x
                    set = True
                    set_num += 1
                    set_size = 0
                    fork_rooms = []
                    counted_rooms = []
                    # count = 0

                    while set == True: #starts to count the set
                        # count += 1 #used for bug testing
                        #works by creating a 'counter' that goes room by room and adds the room to a list of counted rooms
                        c = frame[a][b].test_direction() #gives a list of connections for the room the counter is in
                        t = frame[a - 1][b].test_direction() #gives a list of connections for the room above the counter
                        d = frame[a + 1][b].test_direction() #gives a list of connections for the room below the counter
                        l = frame[a][b - 1].test_direction() #gives a list of connections for the room left of the counter
                        r = frame[a][b + 1].test_direction() #gives a list of connections for the room right of the counter

                        if c['top'] == True and t['bottom'] == True:
                            #moves the counter up
                            #checks to see if there are more than one connections

                            if (c['bottom'] == True and d['top'] == True) or (c['left'] == True and l['right'] == True) or (c['right'] == True and r['left'] == True):
                                #there is so the counter adds the room to a list of fork rooms to return to later
                                frame[a][b].top = False
                                frame[a - 1][b].bottom = False
                                fork_rooms.append(frame[a][b])

                            else:
                                #there isn't so the counter adds it to the counted rooms list and turns the room into a frame room so it won't get counted again
                                counted_rooms.append(frame[a][b])
                                frame[a][b] = copy.deepcopy(frame_room)
                                frame[a][b].y = a
                                frame[a][b].x = b
                                set_size += 1
                            a -= 1

                        elif c['bottom'] == True and d['top'] == True:
                            #moves the counter down

                            if (c['left'] == True and l['right'] == True) or (c['right'] == True and r['left'] == True):
                                frame[a][b].bottom = False
                                frame[a + 1][b].top = False
                                fork_rooms.append(frame[a][b])

                            else:
                                counted_rooms.append(frame[a][b])
                                frame[a][b] = copy.deepcopy(frame_room)
                                frame[a][b].y = a
                                frame[a][b].x = b
                                set_size += 1
                            a += 1

                        elif c['left'] == True and l['right'] == True:
                            #moves the counter left

                            if c['right'] == True and r['left'] == True:
                                frame[a][b].left = False
                                frame[a][b - 1].right = False
                                fork_rooms.append(frame[a][b])

                            else:
                                counted_rooms.append(frame[a][b])
                                frame[a][b] = copy.deepcopy(frame_room)
                                frame[a][b].y = a
                                frame[a][b].x = b
                                set_size += 1
                            b -= 1

                        elif c['right'] == True and r['left'] == True:
                            #moves the counter right
                            counted_rooms.append(frame[a][b])
                            frame[a][b + 1].left = False
                            frame[a][b] = copy.deepcopy(frame_room)
                            frame[a][b].y = a
                            frame[a][b].x = b
                            set_size += 1
                            b += 1

                        else: #this happens when the counter reaches a dead end

                            if len(fork_rooms) > 0:
                                #the counter adds the room it is in then moves back to the last split
                                check = False
                                counted_rooms.append(frame[a][b])
                                frame[a][b] = copy.deepcopy(frame_room)
                                frame[a][b].y = a
                                frame[a][b].x = b

                                while check == False and len(fork_rooms) > 0:
                                    z = fork_rooms.pop() #removes the latest entry to the fork room list and checks to see that it hasn't already been counted
                                    a = z.y
                                    b = z.x

                                    for g in counted_rooms:

                                        if g.x == z.x and g.y == z.y:
                                            #if it is then it breaks the for loop and runs the while loop as long as there are more entries in the fork room list
                                            check = False
                                            break

                                        else:
                                            #if the fork is not already counted then the while loop finishes
                                            check = True

                                else:

                                    if check == True: #if the while loop finished because there is an uncounted fork it adds it and runs from the fork
                                        set_size += 1

                                    elif len(fork_rooms) == 0: #if the while loop finished because there are no more forks then it adds the room it is in then ends the set
                                        set_size += 1
                                        sets[set_num] = counted_rooms
                                        set = False
    
                            else:
                                counted_rooms.append(frame[a][b])
                                frame[a][b] = copy.deepcopy(frame_room)
                                frame[a][b].y = a
                                frame[a][b].x = b
                                set_size += 1
                                sets[set_num] = counted_rooms
                                set = False
                                #ends the set

        return sets
    
    def finishMap(self, max_x, max_y): #function to make the final version of the map
        check = False

        while check == False:
            first_map = self.makeMap(max_x, max_y) #creates a map and tests it
            sets = self.testMap(first_map)

            if len(sets) > 1:
                largest_set = 1

                for x in sets:

                    if len(sets[x]) > len(sets[largest_set]): #finds the largest set in a map
                        largest_set = x

                    else:
                        continue

            else:
                largest_set = 1
            largest_set = sets[largest_set]

            if len(largest_set) < (max_x * max_y) - (2 * (max_x + max_y)) or len(largest_set) > (max_x * max_y) - (max_x + max_y): #checks to see if the map is large enough
                check = False

            else:
                check = True

        else:
            final_map = self.makeArray(max_x, max_y) #makes the frame for the final map

            for a in range(len(first_map)):

                for b in range(len(first_map[a])):

                    for room in largest_set:

                        if room.x == b and room.y == a: #makes the map with only the largest set of rooms
                            final_map[a][b] = copy.deepcopy(first_map[a][b])
                            mon = random.randint(0, 100)
                            if mon > 95:
                                final_map[a][b].monster = True
                            break

                        else:
                            continue
            self.findStart(final_map)
            self.knownRooms.append(self.startRoom)
            return final_map
        
    def findStart(self, finalMap):
        self.startRoom = None
        for y in range(len(finalMap)):
            for x in range(len(finalMap)):
                if finalMap[y][x].rn >= 12 and finalMap[y][x].rn <= 15:
                    self.startRoom = finalMap[y][x]
                    self.currentRoom = finalMap[y][x]
                    break
                if self.startRoom:
                    break
            if self.startRoom:
                break


BR = 715
BL = 605
DB = 415 
L = 580
DT = 250
TR = 1115
TL = 205