from mapGenerator import MapForGame, Room
from characterClass import Character
from monsterClass import Monster
import time, random, sys, os, copy, pickle, logging
from tkinter import *
from tkinter import messagebox

logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)

# make and implement graphical interface
def saveData():
    openFile = open(os.path.join(game.currentPath, "Game.data"), 'wb')
    pickle.dump(game, openFile)
    openFile.close()
    openFile = open(os.path.join(game.currentPath, "Map.data"), 'wb')
    pickle.dump(game.gameMap, openFile)
    openFile.close()
    openFile = open(os.path.join(game.currentPath, "KnownRooms.data"), 'wb')
    pickle.dump(game.knownRooms, openFile)
    openFile.close()
    game.player.pickleSaveChar(os.path.join(game.currentPath, "Character.data"))
    game.currentMonster.pickleSaveMonster(os.path.join(game.currentPath, "CurrentMonster.data"))
    openFile = open(os.path.join(game.currentPath, "Monsters.data"), 'wb')
    pickle.dump(game.monsters, openFile)
    openFile.close()
    app.succesfulTest(XC, DT/2, "Succesfully saved to %s!" % (os.path.basename(game.currentPath)))

class GameWindow(Frame):
    
    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.master = master
        self.initWindow()

    def initWindow(self):
        self.master.title("Game Window")
        self.pack(fill=BOTH, expand=1)
        self.canvas = self.makeCanvas()
        self.mainMenuPage()
        self.master.resizable(1, 1)
        menu = Menu(self.master)
        self.master.config(menu=menu)
        file = Menu(menu)
        file.add_command(label="Save", command=saveData)
        file.add_command(label="Quit", command=self.quitGame)
        menu.add_cascade(label="File", menu=file)
        
    def quitGame(self):
        saveData()
        exit()
    
    def newGamePage(self):
        self.canvas.delete(ALL)
        self.canvas.create_window(BL + (BR - BL)/2, DT + 100, window=Button(master=self.canvas, text="Start New Game", command=self.startnewgame))
        self.filenameEntry = self.makeEntry(self.canvas, "File Name: ", 15)
        self.canvas.create_window(BL + (BR - BL)/2, DT + 150, window=Button(master=self.canvas, text="Back to Main Menu", command=self.mainMenuPage))
    
    def mainMenuPage(self):
        self.canvas.delete(ALL)
        self.canvas.create_window(BL + (BR - BL)/2, DT, window=Label(master=self.canvas, text='WELCOME', font="Helvetica 40"))
        self.canvas.create_window(BL + (BR - BL)/2, DT + (DB - DT)/2, window=Button(master=self.master, text="New", font="Helvetica 20", command=self.newGamePage, pady=10, padx=50)) # make sure to not use "()" otherwise it will call the function once and stop
        self.canvas.create_window(BL + (BR - BL)/2, DT + (DB - DT)/2 + 40, window=Button(master=self.master, text="Load", font="Helvetica 20", command=self.loadGamePage, pady=10, padx=50))
        self.canvas.create_window(BL + (BR - BL)/2, DT + (DB - DT)/2 + 80, window=Button(master=self.master, text="Quit", font="Helvetica 20", command=self.quitGame, pady=10, padx=50))
    
    def loadGamePage(self):
        self.canvas.delete(ALL)
        saveFiles = os.listdir("./SaveData")
        listbox = Listbox(master=self.canvas, height=len(saveFiles), width=10)
        for saveFile in saveFiles:
            if saveFile != ".DS_Store":
                listbox.insert(END, saveFile.replace(".data", ""))
        self.canvas.create_window(BL + (BR - BL)/2, DT, window=listbox)
        self.listbox = listbox
        self.canvas.create_window(BL + (BR - BL)/2, DB + 30 + (len(saveFiles) * 7), window=Button(master=self.canvas, text="Load File", command=self.loadFile))
        self.canvas.create_window(BL + (BR - BL)/2, DB + 60 + (len(saveFiles) * 7), window=Button(master=self.canvas, text="Delete File", command=self.deleteFile))
        self.canvas.create_window(BL + (BR - BL)/2, DB + 90 + (len(saveFiles) * 7), window=Button(master=self.canvas, text="Back to Main Menu", command=self.mainMenuPage))

    def loadFile(self):
        filename = self.listbox.curselection()
        FolderName = str(self.listbox.get(filename))
        CurrentFolder = "./SaveData/" + FolderName
        loadData(CurrentFolder)

    def succesfulTest(self, x, y, message):
        if self.canvas.find_withtag("testMessage") != None:
            self.canvas.delete("testMessage")
        self.canvas.create_window(x, y, window=Label(master=self.canvas, text=message, fg="green", font="Hevelica 20"), tags="testMessage")
    
    def unsuccesfulTest(self, x, y, message):
        if self.canvas.find_withtag("testMessage") != None:
            self.canvas.delete("testMessage")
        self.canvas.create_window(x, y, window=Label(master=self.canvas, text=message, fg="red", font="Hevelica 20"), tags="testMessage")
    
    def deleteFile(self):
        fileIndex = self.listbox.curselection()
        folderName = str(self.listbox.get(fileIndex))
        if messagebox.askyesno("Are You Sure?", "Are you sure you want to delete %s?" % str(self.listbox.get(fileIndex))) == True:
            self.listbox.delete(fileIndex)
            text = "Succesfully deleted %s" % (folderName)
            for filename in os.listdir(os.path.join("SaveData", folderName)):
                os.remove(os.path.join("SaveData", folderName, filename))
            os.rmdir(os.path.join("SaveData", folderName))
            self.loadGamePage()
            self.succesfulTest(BL + (BR - BL)/2, DT/2, text)
            
    def makeEntry(self, parent, caption, width=None, y=250, **options):
        entry = Entry(parent, **options)
        if width:
            entry.config(width=width)
        self.canvas.create_window(BL - (BR - BL)/2, y, window=Label(parent, text=caption))
        self.canvas.create_window(BL + (BR - BL)/2, y, window=entry)
        return entry

    def makeCanvas(self):
        canvas = ResizingCanvas(self.master, width=1325, height=785)
        canvas.pack(fill=BOTH, expand=1, in_=self.master)
        return canvas

    def startnewgame(self):
        filename = str(self.filenameEntry.get())
        filename.replace(" ", "_")
        path = "./SaveData/" + filename
        SaveData = os.listdir("./SaveData")
        if filename in SaveData:
            self.unsuccesfulTest(BL + (BR - BL)/2, DT/2, "That file already exists")
        else:
            self.succesfulTest(BL + (BR - BL)/2, DT/2, "File succesfully created")
            game.startNewGame(filename, path)
    
    def emptyRoomPage(self, directions, text1=None, text2=None):
        if text1 == None:
            text1 = "You have come to an empty room"

        if text2 == None:
            text2 = "Which direction will you go?"

        for x in range(len(directions)):
            if "forward" in directions:
                self.canvas.create_window(XC, LFB, window=Button(master=self.canvas, text="Forwards", command=self.forward, width=9))
                directions.remove("forward")
            
            elif "left" in directions:
                self.canvas.create_window(XC - 60, LFB + 30, window=Button(master=self.canvas, text="Left", command=self.left, width=9))
                directions.remove("left")
        
            elif "right" in directions:
                self.canvas.create_window(XC + 60, LFB + 30, window=Button(master=self.canvas, text="Right", command=self.right, width=9))
                directions.remove("right")
            
            elif "backward" in directions:
                self.canvas.create_window(XC, LFB + 60, window=Button(master=self.canvas, text="Backwards", command=self.backward, width=9))
                directions.remove("backward")

        game.gameMap.currentRoom.drawRoomInterior(self.canvas)
        self.canvas.create_window(XC, L + 20, window=Label(master=self.canvas, text=text1, font="Hevelica 14"))
        self.canvas.create_window(XC, L + 40, window=Label(master=self.canvas, text=text2, font="Hevelica 14"))
        self.canvas.create_window(80, LFB, window=Button(master=self.canvas, text="Map", command=drawKnownMap, width=12))
        self.canvas.create_window(80, LFB + 30, window=Button(master=self.canvas, text="Player Stats", command=self.playerStatPage, width=12))
        self.canvas.create_window(80, LFB - 30, window=Button(master=self.canvas, text="Save Game", command=game.saveGameData, width=12))
        game.gameMap.drawMiniMap(self.canvas, game.gameMap.gameMap, game.knownRooms, game.player.cords, side=game.face)

    def monsterInteractionPage(self):
        game.currentMonster.drawMonster(self.canvas)
        text1 = "You have encountered a Monster!"
        text2 = "What will you do?"

        self.canvas.create_window(XC, LFB, window=Button(master=self.canvas, text="Fight", command=self.fightMonster, width=9))
        self.canvas.create_window(XC, LFB + 30, window=Button(master=self.canvas, text="Run", command=self.runFromFight, width=9))
        self.canvas.create_window(XC, LFB + 60, window=Button(master=self.canvas, text="Monster Stats", command=self.monsterStatPage, width=9))
        self.canvas.create_window(XC, L + 20, window=Label(master=self.canvas, text=text1, font="Hevelica 14"))
        self.canvas.create_window(XC, L + 40, window=Label(master=self.canvas, text=text2, font="Hevelica 14"))
        self.canvas.create_window(80, LFB, window=Button(master=self.canvas, text="Map", command=drawKnownMap, width=12))
        self.canvas.create_window(80, LFB + 30, window=Button(master=self.canvas, text="Player Stats", command=self.playerStatPage, width=12))
        self.canvas.create_window(80, LFB - 30, window=Button(master=self.canvas, text="Save Game", command=game.saveGameData, width=12))

    def fightMonster(self):
        pass

    def runFromFight(self):
        self.canvas.delete(ALL)
        directions = ["backward"]
        self.emptyRoomPage(directions, text1="You have run from the monster", text2="There is only one direction to run")
        game.currentMonster.escaped = True

    def monsterStatPage(self):
        self.canvas.delete(ALL)

        nameLabel = Label(self.canvas, text="Name:", anchor=E, font="Hevelica 14", width=12)
        attackLabel = Label(self.canvas, text="Attack:", anchor=E, font="Hevelica 14", width=12)
        defenceLabel = Label(self.canvas, text="Defence:", anchor=E, font="Hevelica 14", width=12)
        agilityLabel = Label(self.canvas, text="Agility:", anchor=E, font="Hevelica 14", width=12)
        hpLabel = Label(self.canvas, text="Hit Points:", anchor=E, font="Hevelica 14", width=12)
        levelLabel = Label(self.canvas, text="Level:", anchor=E, font="Hevelica 14", width=12)

        width = len(game.currentMonster.name)
        nameValue = Label(self.canvas, text=game.currentMonster.name, anchor=W, font="Hevelica 14", width=width)
        attackValue = Label(self.canvas, text=str(game.currentMonster.attack), anchor=W, font="Hevelica 14", width=width)
        defenceValue = Label(self.canvas, text=str(game.currentMonster.defence), anchor=W, font="Hevelica 14", width=width)
        agilityValue = Label(self.canvas, text=str(game.currentMonster.agility), anchor=W, font="Hevelica 14", width=width)
        hpValue = Label(self.canvas, text="%s / %s" % (game.currentMonster.hpC, game.currentMonster.hpM), anchor=W, font="Hevelica 14", width=width)
        levelValue = Label(self.canvas, text=str(game.currentMonster.level), anchor=W, font="Hevelica 14", width=width)

        self.canvas.create_window(XC - XC/5, DT, window=nameLabel)
        self.canvas.create_window(XC, DT, window=nameValue)

        self.canvas.create_window(XC - XC/5, DT + 30, window=levelLabel)
        self.canvas.create_window(XC, DT + 30, window=levelValue)

        self.canvas.create_window(XC - XC/5, DT + 60, window=hpLabel)
        self.canvas.create_window(XC, DT + 60, window=hpValue)
        
        self.canvas.create_window(XC - XC/5, DT + 90, window=attackLabel)
        self.canvas.create_window(XC, DT + 90, window=attackValue)

        self.canvas.create_window(XC - XC/5, DT + 120, window=defenceLabel)
        self.canvas.create_window(XC, DT + 120, window=defenceValue)

        self.canvas.create_window(XC - XC/5, DT + 150, window=agilityLabel)
        self.canvas.create_window(XC, DT + 150, window=agilityValue)

        self.canvas.create_window(80, LFB, window=Button(self.canvas, text="Back", command=self.enterRoom, width=12))
        self.canvas.create_window(80, LFB + 30, window=Button(master=self.canvas, text="Map", command=drawKnownMap, width=12))
        self.canvas.create_window(80, LFB - 30, window=Button(master=self.canvas, text="Save Game", command=game.saveGameData, width=12))

    def playerStatPage(self):
        self.canvas.delete(ALL)
        
        nameLabel = Label(self.canvas, text="Name:", anchor=E, font="Hevelica 14", width=12)
        attackLabel = Label(self.canvas, text="Attack:", anchor=E, font="Hevelica 14", width=12)
        defenceLabel = Label(self.canvas, text="Defence:", anchor=E, font="Hevelica 14", width=12)
        agilityLabel = Label(self.canvas, text="Agility:", anchor=E, font="Hevelica 14", width=12)
        hpLabel = Label(self.canvas, text="Hit Points:", anchor=E, font="Hevelica 14", width=12)
        xpLabel = Label(self.canvas, text="XP:", anchor=E, font="Hevelica 14", width=12)
        levelLabel = Label(self.canvas, text="Level:", anchor=E, font="Hevelica 14", width=12)

        width = len(game.player.name)
        nameValue = Label(self.canvas, text=game.player.name, anchor=W, font="Hevelica 14", width=width)
        attackValue = Label(self.canvas, text=str(game.player.attack), anchor=W, font="Hevelica 14", width=width)
        defenceValue = Label(self.canvas, text=str(game.player.defence), anchor=W, font="Hevelica 14", width=width)
        agilityValue = Label(self.canvas, text=str(game.player.agility), anchor=W, font="Hevelica 14", width=width)
        hpValue = Label(self.canvas, text="%s / %s" % (game.player.hp_c, game.player.hp_m), anchor=W, font="Hevelica 14", width=width)
        xpValue = Label(self.canvas, text="%s / %s" % (game.player.xp_c, game.player.xp_n), anchor=W, font="Hevelica 14", width=width)
        levelValue = Label(self.canvas, text=str(game.player.level), anchor=W, font="Hevelica 14", width=width)

        self.canvas.create_window(XC - XC/5, DT, window=nameLabel)
        self.canvas.create_window(XC, DT, window=nameValue)

        self.canvas.create_window(XC - XC/5, DT + 30, window=levelLabel)
        self.canvas.create_window(XC, DT + 30, window=levelValue)

        self.canvas.create_window(XC - XC/5, DT + 60, window=xpLabel)
        self.canvas.create_window(XC, DT + 60, window=xpValue)

        self.canvas.create_window(XC - XC/5, DT + 90, window=hpLabel)
        self.canvas.create_window(XC, DT + 90, window=hpValue)
        
        self.canvas.create_window(XC - XC/5, DT + 120, window=attackLabel)
        self.canvas.create_window(XC, DT + 120, window=attackValue)

        self.canvas.create_window(XC - XC/5, DT + 150, window=defenceLabel)
        self.canvas.create_window(XC, DT + 150, window=defenceValue)

        self.canvas.create_window(XC - XC/5, DT + 180, window=agilityLabel)
        self.canvas.create_window(XC, DT + 180, window=agilityValue)

        self.canvas.create_window(80, LFB, window=Button(self.canvas, text="Back", command=self.enterRoom, width=12))
        self.canvas.create_window(80, LFB + 30, window=Button(master=self.canvas, text="Map", command=drawKnownMap, width=12))
        self.canvas.create_window(80, LFB - 30, window=Button(master=self.canvas, text="Save Game", command=game.saveGameData, width=12))
        
    def forward(self):
        game.gameMap.currentRoom.player = False
        # text = ""
        if game.face == "north":
            # go north
            game.gameMap.currentRoom = game.gameMap.gameMap[game.gameMap.currentRoom.y - 1][game.gameMap.currentRoom.x]

        elif game.face == "south":
            # go south
            game.gameMap.currentRoom = game.gameMap.gameMap[game.gameMap.currentRoom.y + 1][game.gameMap.currentRoom.x]

        elif game.face == "east":
            # go east
            game.gameMap.currentRoom = game.gameMap.gameMap[game.gameMap.currentRoom.y][game.gameMap.currentRoom.x + 1]
            
        elif game.face == "west":
            # go west
            game.gameMap.currentRoom = game.gameMap.gameMap[game.gameMap.currentRoom.y][game.gameMap.currentRoom.x - 1]

        text = "Forward button pressed, going %s" % (game.face)
        game.gameMap.currentRoom.player = True
        for room in game.knownRooms:
            if game.gameMap.currentRoom.x == room['x'] and game.gameMap.currentRoom.y == room['y']:
                break
        else:
            newRoom = {'x': game.gameMap.currentRoom.x, 'y': game.gameMap.currentRoom.y}
            game.knownRooms.append(newRoom)
        
        game.player.movement(game.face)
        if game.currentMonster != None:
            if game.currentMonster.escaped == True:
                game.currentMonster.escaped = False
        self.enterRoom()
        self.succesfulTest(XC, DT/2, text)

    def backward(self):
        game.gameMap.currentRoom.player = False
        if game.face == "north":
            # go south
            game.gameMap.currentRoom = game.gameMap.gameMap[game.gameMap.currentRoom.y + 1][game.gameMap.currentRoom.x]
            game.face = "south"

        elif game.face == "south":
            # go north
            game.gameMap.currentRoom = game.gameMap.gameMap[game.gameMap.currentRoom.y - 1][game.gameMap.currentRoom.x]
            game.face = "north"

        elif game.face == "east":
            # go west
            game.gameMap.currentRoom = game.gameMap.gameMap[game.gameMap.currentRoom.y][game.gameMap.currentRoom.x - 1]
            game.face = "west"

        elif game.face == "west":
            # go east
            game.gameMap.currentRoom = game.gameMap.gameMap[game.gameMap.currentRoom.y][game.gameMap.currentRoom.x + 1]
            game.face = "east"
            
        text = "Backward button pressed, going %s" % (game.face)
        game.gameMap.currentRoom.player = True
        for room in game.knownRooms:
            if game.gameMap.currentRoom.x == room['x'] and game.gameMap.currentRoom.y == room['y']:
                break
        else:
            newRoom = {'x': game.gameMap.currentRoom.x, 'y': game.gameMap.currentRoom.y}
            game.knownRooms.append(newRoom)
        
        game.player.movement(game.face)
        if game.currentMonster != None:
            if game.currentMonster.escaped == True:
                game.currentMonster.escaped = False
        self.enterRoom()
        self.succesfulTest(XC, DT/2, text)

    def left(self):
        # self.succesfulTest(XC, DT/2, "Left button works")
        game.gameMap.currentRoom.player = False
        if game.face == "north":
            # go west
            game.gameMap.currentRoom = game.gameMap.gameMap[game.gameMap.currentRoom.y ][game.gameMap.currentRoom.x - 1]
            game.face = "west"

        elif game.face == "south":
            # go east
            game.gameMap.currentRoom = game.gameMap.gameMap[game.gameMap.currentRoom.y][game.gameMap.currentRoom.x + 1]
            game.face = "east"

        elif game.face == "east":
            # go north
            game.gameMap.currentRoom = game.gameMap.gameMap[game.gameMap.currentRoom.y - 1][game.gameMap.currentRoom.x]
            game.face = "north"

        elif game.face == "west":
            # go south
            game.gameMap.currentRoom = game.gameMap.gameMap[game.gameMap.currentRoom.y + 1][game.gameMap.currentRoom.x]
            game.face = "south"

        text = "Left button pressed, going %s" % (game.face)
        game.gameMap.currentRoom.player = True
        for room in game.knownRooms:
            if game.gameMap.currentRoom.x == room['x'] and game.gameMap.currentRoom.y == room['y']:
                break
        else:
            newRoom = {'x': game.gameMap.currentRoom.x, 'y': game.gameMap.currentRoom.y}
            game.knownRooms.append(newRoom)

        game.player.movement(game.face)
        if game.currentMonster != None:
            if game.currentMonster.escaped == True:
                game.currentMonster.escaped = False
        self.enterRoom()
        self.succesfulTest(XC, DT/2, text)

    def right(self):
        # self.succesfulTest(XC, DT/2, "Right button works")
        game.gameMap.currentRoom.player = False
        if game.face == "north":
            # go east
            game.gameMap.currentRoom = game.gameMap.gameMap[game.gameMap.currentRoom.y][game.gameMap.currentRoom.x + 1]
            game.face = "east"

        elif game.face == "south":
            # go west
            game.gameMap.currentRoom = game.gameMap.gameMap[game.gameMap.currentRoom.y][game.gameMap.currentRoom.x - 1]
            game.face = "west"

        elif game.face == "east":
            # go south
            game.gameMap.currentRoom = game.gameMap.gameMap[game.gameMap.currentRoom.y + 1][game.gameMap.currentRoom.x]
            game.face = "south"

        elif game.face == "west":
            # go north
            game.gameMap.currentRoom = game.gameMap.gameMap[game.gameMap.currentRoom.y - 1][game.gameMap.currentRoom.x]
            game.face = "north"

        text = "Right button pressed, going %s" % (game.face)
        game.gameMap.currentRoom.player = True
        for room in game.knownRooms:
            if game.gameMap.currentRoom.x == room['x'] and game.gameMap.currentRoom.y == room['y']:
                break
        else:
            newRoom = {'x': game.gameMap.currentRoom.x, 'y': game.gameMap.currentRoom.y}
            game.knownRooms.append(newRoom)

        game.player.movement(game.face)
        if game.currentMonster != None:
            if game.currentMonster.escaped == True:
                game.currentMonster.escaped = False
        self.enterRoom()
        self.succesfulTest(XC, DT/2, text)
    
    def enterRoom(self):
        logging.debug("started enterRoom")
        self.canvas.delete(ALL)
        directions = game.gameMap.currentRoom.test_direction_for_drawing(side=game.face)
        if game.gameMap.currentRoom.monster == True:
            for mon in game.monsters:
                if game.gameMap.currentRoom.x == mon.cords['x'] and game.gameMap.currentRoom.y == mon.cords['y']:
                    game.currentMonster = mon 
                    break
            game.gameMap.currentRoom.drawRoomInterior(self.canvas)
            self.monsterInteractionPage()
        else:
            self.emptyRoomPage(directions)
            game.gameMap.currentRoom.drawRoomInterior(self.canvas, side=game.face)
            self.makeCompass(side=game.face)
        logging.debug("finished enterRoom")
    
    def makeCompass(self, side=None):
        # Make the compassRose
        p = 42
        t = 10
        x = TR - TL/2
        y = LFB + 30
        points = []
        for i in (1, -1):
            points.extend((x, y + i*p))
            points.extend((x + i*t, y + i*t))
            points.extend((x + i*p, y))
            points.extend((x + i*t, y - i*t))
        
        self.canvas.create_polygon(points)
        points = []
        for i in (1, -1):
            points.extend((x, y + i*p + 10*i))
            points.extend((x + i*p + 10*i, y))
        if side == "north":
            # put letters normally
            self.canvas.create_window(points[0], points[1], window=Label(master=self.canvas, text="N", font="Helvetica 10"))
            self.canvas.create_window(points[2], points[3], window=Label(master=self.canvas, text="E", font="Helvetica 10"))
            self.canvas.create_window(points[4], points[5], window=Label(master=self.canvas, text="S", font="Helvetica 10"))
            self.canvas.create_window(points[6], points[7], window=Label(master=self.canvas, text="W", font="Helvetica 10"))

        elif side == "east":
            # put letters rotated counter clockwise once
            self.canvas.create_window(points[0], points[1], window=Label(master=self.canvas, text="E", font="Helvetica 10"))
            self.canvas.create_window(points[2], points[3], window=Label(master=self.canvas, text="S", font="Helvetica 10"))
            self.canvas.create_window(points[4], points[5], window=Label(master=self.canvas, text="W", font="Helvetica 10"))
            self.canvas.create_window(points[6], points[7], window=Label(master=self.canvas, text="N", font="Helvetica 10"))
        
        elif side == "south":
            # put letters rotated counter clockwise twice
            self.canvas.create_window(points[0], points[1], window=Label(master=self.canvas, text="S", font="Helvetica 10"))
            self.canvas.create_window(points[2], points[3], window=Label(master=self.canvas, text="W", font="Helvetica 10"))
            self.canvas.create_window(points[4], points[5], window=Label(master=self.canvas, text="N", font="Helvetica 10"))
            self.canvas.create_window(points[6], points[7], window=Label(master=self.canvas, text="E", font="Helvetica 10"))

        elif side == "west":
            # put letters rotated counter clockwise three times
            self.canvas.create_window(points[0], points[1], window=Label(master=self.canvas, text="W", font="Helvetica 10"))
            self.canvas.create_window(points[2], points[3], window=Label(master=self.canvas, text="N", font="Helvetica 10"))
            self.canvas.create_window(points[4], points[5], window=Label(master=self.canvas, text="E", font="Helvetica 10"))
            self.canvas.create_window(points[6], points[7], window=Label(master=self.canvas, text="S", font="Helvetica 10"))

    def characterCreationPage(self):
        logging.debug("started characterCreationPage")
        self.canvas.delete(ALL)
        nameEntry = self.makeEntry(self.canvas, "Name: ", width=10, y=DT-DT/4)
        attackVar = StringVar()
        defenceVar = StringVar()
        agilityVar = StringVar()
        hpVar = StringVar()
        pointVar = StringVar()

        attackVar.set("1")
        defenceVar.set("1")
        agilityVar.set("1")
        hpVar.set("1")
        pointVar.set("40")

        def _attackDecrease():
            if int(attackVar.get()) <= 1:
                self.unsuccesfulTest(XC, L, "You can't have an attack lower than 1")
            else:
                attackVar.set(str(int(attackVar.get()) - 1))
                pointVar.set(str(int(pointVar.get()) + 1))

        def _attackIncrease():
            if int(pointVar.get()) > 0:
                attackVar.set(str(int(attackVar.get()) + 1))
                pointVar.set(str(int(pointVar.get()) - 1))
            else:
                self.unsuccesfulTest(XC, L, "You don't have enough points to increase that trait")

        def _defenceDecrease():
            if int(defenceVar.get()) <= 1:
                self.unsuccesfulTest(XC, L, "You can't have a defence lower than 1")
            else:
                defenceVar.set(str(int(defenceVar.get()) - 1))
                pointVar.set(str(int(pointVar.get()) + 1))

        def _defenceIncrease():
            if int(pointVar.get()) > 0:
                defenceVar.set(str(int(defenceVar.get()) + 1))
                pointVar.set(str(int(pointVar.get()) - 1))
            else:
                self.unsuccesfulTest(XC, L, "You don't have enough points to increase that trait")

        def _agilityDecrease():
            if int(agilityVar.get()) <= 1:
                self.unsuccesfulTest(XC, L, "You can't have an agility lower than 1")
            else:
                agilityVar.set(str(int(agilityVar.get()) - 1))
                pointVar.set(str(int(pointVar.get()) + 1))

        def _agilityIncrease():
            if int(pointVar.get()) > 0:
                agilityVar.set(str(int(agilityVar.get()) + 1))
                pointVar.set(str(int(pointVar.get()) - 1))
            else:
                self.unsuccesfulTest(XC, L, "You don't have enough points to increase that trait")

        def _hpDecrease():
            if int(hpVar.get()) <= 1:
                self.unsuccesfulTest(XC, L, "You can't have less than 1 Hit Point")
            else:
                hpVar.set(str(int(hpVar.get()) - 1))
                pointVar.set(str(int(pointVar.get()) + 1))

        def _hpIncrease():
            if int(pointVar.get()) > 0:
                hpVar.set(str(int(hpVar.get()) + 1))
                pointVar.set(str(int(pointVar.get()) - 1))
            else:
                self.unsuccesfulTest(XC, L, "You don't have enough points to increase that trait")

        def _finishedCharCreation():
            if int(pointVar.get()) > 0:
                if messagebox.askyesno("Are you sure?", "Are you sure you want to continue? \nYou still have %s points left." % (pointVar.get())) == True:
                    # make the character
                    player = Character(nameEntry.get(), int(attackVar.get()), int(defenceVar.get()), int(agilityVar.get()), int(hpVar.get()), None, [game.gameMap.startRoom.y, game.gameMap.startRoom.x])
                    game.player = player
                    self.enterRoom()
                    saveData()

                else:
                    pass
                    # break and restart

            else: 
                # make the character
                player = Character(nameEntry.get(), int(attackVar.get()), int(defenceVar.get()), int(agilityVar.get()), int(hpVar.get()), None, [game.gameMap.startRoom.y, game.gameMap.startRoom.x])
                game.player = player
                self.enterRoom()
                saveData()

        attackLabel = Label(self.canvas, text="Attack: ", anchor=E, font="Hevelica 14")
        defenceLabel = Label(self.canvas, text="Defence: ", anchor=E, font="Hevelica 14")
        agilityLabel = Label(self.canvas, text="Agility: ", anchor=E, font="Hevelica 14")
        hpLabel = Label(self.canvas, text="Hit Points: ", anchor=E, font="Hevelica 14")
        pointLabel = Label(self.canvas, text="Points Left: ", anchor=E, font="Hevelica 14")

        attackNum = Label(self.canvas, textvariable=attackVar, font="Hevelica 14")
        defenceNum = Label(self.canvas, textvariable=defenceVar, font="Hevelica 14")
        agilityNum = Label(self.canvas, textvariable=agilityVar, font="Hevelica 14")
        hpNum = Label(self.canvas, textvariable=hpVar, font="Hevelica 14")
        pointNum = Label(self.canvas, textvariable=pointVar, font="Hevelica 14")

        self.canvas.create_window(XC - XC/5, DT/2, window=pointLabel)
        self.canvas.create_window(XC, DT/2, window=pointNum)

        self.canvas.create_window(XC - XC/5, DT, window=attackLabel)
        self.canvas.create_window(XC - 30, DT, window=Button(master=self.canvas, text="<", command=_attackDecrease, font="Hevelica 14", width=1))
        self.canvas.create_window(XC, DT, window=attackNum)
        self.canvas.create_window(XC + 30, DT, window=Button(master=self.canvas, text=">", command=_attackIncrease, font="Hevelica 14", width=1))

        self.canvas.create_window(XC - XC/5, DT + 30, window=defenceLabel)
        self.canvas.create_window(XC - 30, DT + 30, window=Button(master=self.canvas, text="<", command=_defenceDecrease, font="Hevelica 14", width=1))
        self.canvas.create_window(XC, DT + 30, window=defenceNum)
        self.canvas.create_window(XC + 30, DT + 30, window=Button(master=self.canvas, text=">", command=_defenceIncrease, font="Hevelica 14", width=1))

        self.canvas.create_window(XC - XC/5, DT + 60, window=agilityLabel)
        self.canvas.create_window(XC - 30, DT + 60 , window=Button(master=self.canvas, text="<", command=_agilityDecrease, font="Hevelica 14", width=1))
        self.canvas.create_window(XC, DT + 60, window=agilityNum)
        self.canvas.create_window(XC + 30, DT + 60, window=Button(master=self.canvas, text=">", command=_agilityIncrease, font="Hevelica 14", width=1))

        self.canvas.create_window(XC - XC/5, DT + 90, window=hpLabel)
        self.canvas.create_window(XC - 30, DT + 90, window=Button(master=self.canvas, text="<", command=_hpDecrease, font="Hevelica 14", width=1))
        self.canvas.create_window(XC, DT + 90, window=hpNum)
        self.canvas.create_window(XC + 30, DT + 90, window=Button(master=self.canvas, text=">", command=_hpIncrease, font="Hevelica 14", width=1))

        self.canvas.create_window(XC, LFB, window=Button(master=self.canvas, text="Continue?", command=_finishedCharCreation, font="Hevelica 14"))

        

def pressedQuit():
    quit()

app = None
root = None

def makeApp():
    global app
    global root
    root = Tk()
    root.geometry("1320x780")
    root.minsize(width=990, height=585)
    app = GameWindow(root)
    return root, app
class Game(object):

    def __init__(self):
        self.initGame()

    def initGame(self):
        makeApp()
        app.mainMenuPage()
        
    def startNewGame(self, filename, path):
        os.makedirs(path)
        openFile = open(os.path.join(path, "Game.data"), "wb")
        openFile.close()
        openFile = open(os.path.join(path, "Map.data"), "wb")
        openFile.close()
        openFile = open(os.path.join(path, "KnownRooms.data"), "wb")
        openFile.close()
        openFile = open(os.path.join(path, "Character.data"), "wb")
        openFile.close()
        openFile = open(os.path.join(path, "Monsters.data"), "wb")
        openFile.close()
        openFile = open(os.path.join(path, "CurrentMonster.data"), 'wb')
        openFile.close()
        logging.debug("New folder named %s made at %s" % (filename, path))
        self.currentFolder = filename
        self.currentPath = path
        self.gameMap = MapForGame()
        self.knownRooms = [{'x':self.gameMap.startRoom.x, 'y':self.gameMap.startRoom.y}]
        self.player = Character("Default", 1, 2, 3, 4, 5, [6, 7])
        if self.gameMap.startRoom.rn == 12:
            self.face = "north"

        elif self.gameMap.startRoom.rn == 13:
            self.face = "south"

        elif self.gameMap.startRoom.rn == 14:
            self.face = "east"

        elif self.gameMap.startRoom.rn == 15:
            self.face = "west"
        
        else:
            logging.info("startRoom.rn was not 12, 13, 14, or 15; setting side as 'north'")
            self.face = "north"
        self.gameMap.startRoom.player = True
        self.gameMap.startRoom.monster = True
        self.monsters = []
        for y in range(len(self.gameMap.gameMap)):
            for x in range(len(self.gameMap.gameMap[y])):
                if self.gameMap.gameMap[y][x].monster == True:
                    self.monsters.append(Monster("Skelly", 1, {'x':x, 'y':y}, app.canvas))
        self.currentMonster = None
        app.characterCreationPage()

    def finishedCharCreation(self):
        app.enterRoom()
        saveData()
        
    def loadGameData(self, openFile):
        logging.debug("started loadGameData")
        game = pickle.load(openFile)
        openFile.close()
        logging.debug("finished loadGameData")
        return game

    def loadMapData(self, openFile):
        logging.debug("started loadMapData")
        gameMap = pickle.load(openFile)
        openFile.close()
        logging.debug("finished loadMapData")
        return gameMap

    def loadKnownRooms(self, openFile):
        logging.debug("started loadKnownRooms")
        knownRooms = pickle.load(openFile)
        openFile.close()
        logging.debug("finished loadKnownRooms")
        return knownRooms

    def loadMonsters(self, openFile):
        logging.debug("started loadMonsters")
        monsters = pickle.load(openFile)
        openFile.close()
        logging.debug("finished loadMonsters")
        return monsters
    
    # def loadCharacterData(self, openFile):
    #     logging.debug("started loadCharacterData")
    #     player = pickle.load(openFile)
    #     openFile.close()
    #     logging.debug("finished loadCharacterData")
    #     return player

    def saveGameData(self):
        saveData()


class ResizingCanvas(Canvas):
    def __init__(self, parent, **kwargs):
        Canvas.__init__(self, parent, **kwargs)
        self.bind("<Configure>", self.on_resize)
        self.height = self.winfo_reqheight()
        self.width = self.winfo_reqwidth()

    def on_resize(self, event):
        wscale = float(event.width)/self.width
        hscale = float(event.height)/self.height
        self.width = event.width
        self.height = event.height
        self.config(width=self.width, height=self.height)
        self.scale("all", 0, 0, wscale, hscale)

BR = 715
BL = 605
DB = 415 
L = 580
DT = 250
TR = 1115
TL = 205
XC = BL + (BR - BL)/2
LFB = L + 80

game = Game()
    
def drawKnownMap():
    logging.debug("started drawKnownMap")
    logging.debug("attempting to call game.gameMap.drawKnownRooms")
    game.gameMap.drawKnownRooms(app.canvas, game.gameMap.gameMap, game.knownRooms, game.face)
    app.canvas.create_window(80, LFB, window=Button(master=app.canvas, text="Back", command=app.enterRoom, width=12))
    app.canvas.create_window(80, LFB - 30, window=Button(master=app.canvas, text="Save Game", command=game.saveGameData, width=12))
    app.canvas.create_window(80, LFB + 30, window=Button(master=app.canvas, text="Player Stats", command=app.playerStatPage, width=12))
    logging.debug("finished drawKnownMap")

def loadData(DataFolder):
    logging.debug("started 'loadData(%s)'" % (DataFolder))
    global game
    try:
        currentFilePath = os.path.join(DataFolder, "Game.data")
        openFile = open(currentFilePath, 'rb')
        logging.debug("have the currentFilePath and the openFile defined, attempting to loadGameData")
        game = Game.loadGameData(None, openFile)
        currentFilePath = os.path.join(DataFolder, "Map.data")
        openFile = open(currentFilePath, 'rb')
        logging.debug("have the currentFilePath and the openFile defined, attempting to loadMapData")
        gameMap = game.loadMapData(openFile)
        currentFilePath = os.path.join(DataFolder, "KnownRooms.data")
        openFile = open(currentFilePath, 'rb')
        logging.debug("have the currentFilePath and the openFile defined, attempting to loadKnownRooms")
        knownRooms = game.loadKnownRooms(openFile)
        currentFilePath = os.path.join(DataFolder, "Character.data")
        # openFile = open(currentFilePath, 'rb')
        logging.debug("have the currentFilePath and the openFile defined, attempting to loadCharacterData")
        player = Character.pickleLoadChar(None, currentFilePath)
        currentFilePath = os.path.join(DataFolder, "CurrentMonster.data")
        currentMonster = Monster.pickleLoadMonster(None, currentFilePath)
        currentFilePath = os.path.join(DataFolder, "Monsters.data")
        openFile = open(currentFilePath, 'rb')
        monsters = game.loadMonsters(openFile)
        game.monsters = monsters
        game.currentMonster = currentMonster
        game.player = player
        game.gameMap = gameMap
        game.knownRooms = knownRooms
        logging.debug("attempting to enterRoom")
        app.enterRoom()
        logging.debug("finished loadData()")
    except AttributeError or NameError as error:
        logging.warning("An error occured because: %s" % (error))
        quit()


# Make start screen

# generate map and place player

root.mainloop()

# TODO: make and implement player interactions with new empty room

# TODO: make and implement player interactions with shop

# TODO: make and implement player interactions with monsters

# TODO: make and implement player interactions with previously visited rooms

# TODO: have a way to save the game and pick up again from a save file

