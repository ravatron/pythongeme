import pickle, random, os, time
class Character(object):
    def __init__(self, name, attack, defence, agility, hp_c, weapon, cords, level=1, xp_c=0, inventory=[]):
        self.name = name
        self.attack = attack
        self.defence = defence
        self.agility = agility
        self.hp_c = hp_c
        self.level = level
        self.xp_c = xp_c
        self.gold = 10
        self.weapon = weapon
        self.hp_m = hp_c
        self.xp_n = self.calcNextLevel()
        self.inventory = inventory
        self.cords = cords

    def calcNextLevel(self):
        # each level needs 150% as much xp as the previous 
        nxtLvl = 25
        if self.level > 1:
            for lvl in range(self.level):
                nxtLvl += int(nxtLvl/2)
        return nxtLvl
    
    # def __new__(cls, name='Default', attack=11, defence=12, agility=13, hp_c=14, level=15, xp_c=16, gold=17, weapon=18, hp_m=19, xp_n=20):
        # self = super().__new__(cls)
        # self.name = name
        # self.attack = attack
        # self.defence = defence
        # self.agility = agility
        # self.hp_c = hp_c
        # self.level = level
        # self.xp_c = xp_c
        # self.gold = gold
        # self.weapon = weapon
        # self.hp_m = hp_m
        # self.xp_n = xp_n
        # return self
        
    # def print_stats(self):
    #     print ("Name:       {}".format(str(self.name)))
    #     print ("Level:      {}".format(str(self.level)))
    #     print ("XP:         {}/{}".format(str(self.xp_c), str(self.xp_n)))
    #     print ("Hit Points: {}/{}".format(str(self.hp_c),str(self.hp_m)))
    #     print ("Attack:     {}".format(str(self.attack)))
    #     print ("Defence:    {}".format(str(self.defence)))
    #     print ("Agility:    {}".format(str(self.agility)))
    #     print ("Gold:       {}".format(str(self.gold)))

    # def char_creation(self):
    #     yn = 1
    #     print ("Hi there!")
    #     print ("I'm the narrator, and you are?")
    #     self.name = input("My name is: ")
    #     print ("Nice to meet you {}!".format(self.name))
    #     print ("I guess you want to make your character now...")
    #     time.sleep(0.5)
    #     print ("Would you like me to teach you how? (y/n)")
    #     while True:
    #         yn = input('>>> ')
    #         yn = yn.lower()
    #         if yn == "y" or yn == "yes":
    #             #run tutorial
    #             print ("You have 20 points to spend on 4 areas on your character")
    #             print ("You can spend it on Attack, Defence, Agility, and HP") # Need to have explinations for what each aspect does
    #             print ("You assign these points by either typing out the area or it's corrisponding number")
    #             print ("As you level up you will be able to add extra points into these fields")
    #             break
    #         elif yn == "n" or yn == "no":
    #             print ("Oh, I guess you don't need my help")
    #             print ("Thats fine, I'm not upset or anything")
    #             #skip tutorial
    #             break
    #         else:
    #             print ("My programmer was lazy and didn't acount for you to say that")
    #             print ("Answer again, this time using either 'y' or 'n'")
    #             #say something about not being a complex to understand
    #     cpoints = 20
    #     self.attack = 0
    #     self.defence = 0
    #     self.agility = 0
    #     self.hp_m = 0
    #     while True:
    #         print ("You have {} points left, how would you like to spend them?".format(str(cpoints)))
    #         print ("1)  Attack :  {}".format(str(self.attack)))
    #         print ("2)  Defence:  {}".format(str(self.defence)))
    #         print ("3)  Agility:  {}".format(str(self.agility)))
    #         print ("4)  Max HP :  {}".format(str(self.hp_m)))
    #         action = input("Spend it on: ")
    #         action = action.lower()
    #         if action == "1" or action == "attack":
    #             #edit Attack
    #             self.attack += 1
    #             cpoints -= 1
    #         elif action == "2" or action == "defence":
    #             #edit Defence
    #             self.defence += 1
    #             cpoints -= 1
    #         elif action == "3" or action == "agility":
    #             #edit Agility
    #             self.agility += 1
    #             cpoints -= 1
    #         elif action == "4" or action ==  "hp" or action == "max hp" or action == "maxhp":
    #             #edit HP
    #             self.hp_m += 1
    #             cpoints -= 1
    #         else:
    #             print ("Either spell it correctly or use the corresponding number")
    #         if cpoints == 0:
    #             print ("Attack :  {}".format(str(self.attack)))
    #             print ("Defence:  {}".format(str(self.defence)))
    #             print ("Agility:  {}".format(str(self.agility)))
    #             print ("Max HP :  {}".format(str(self.hp_m)))
    #             print ("Are you happy with these stats? (y/n)")
    #             while True:
    #                 action = input('>>> ')
    #                 action = action.lower()
    #                 if action == "y" or action == "yes":
    #                     self.hp_c = self.hp_m
    #                     self.level = 1
    #                     self.xp_n = 25
    #                     break
    #                 elif action == "n" or action == "no":
    #                     cpoints = 20
    #                     self.attack = 0
    #                     self.defence = 0
    #                     self.agility = 0
    #                     self.hp_m = 0
    #                     break
    #                 else:
    #                     print ("My programmer was lazy and didn't acount for you to say that")
    #                     print ("Answer again, this time using either 'y' or 'n'")
    #     print ("Now what is your weapon of choice?")
    #     print ("Is it a Sword? (1)")
    #     print ("Is it a Bow?   (2)")
    #     print ("Is it a Dagger?(3)")
    #     print ("The Sword  does moderate damage, has high accuracy,     and has a low chance to crit")
    #     print ("The Bow    does high damage,     has low accuracy,      and has a moderate chance to crit")
    #     print ("The Dagger does low damage,      has moderate accuracy, and has a high chance to crit")
    #     while True:
    #         action = input("I want the: ")
    #         action = action.lower()
    #         if action == "1" or action == "sword":
    #             self.weapon = 1
    #             break
    #         elif action == "2" or action == "bow":
    #             self.weapon = 2
    #             break
    #         elif action == "3" or action == "dagger":
    #             self.weapon = 3
    #             break
    #         else:
    #             print ("Sorry can you say that again?")
    #             print ("This time spell it right? (or select the corrisponding number)")

    # def level_up(self):
    #     self.xp_c = self.xp_c - self.xp_n
    #     print("Looks like you got enough XP to level up!")
    #     print("What would you like to spend your level up point on?")
    #     print("1) +2 to Attack")
    #     print("2) +2 to Defence")
    #     print("3) +2 to Agility")
    #     print("4) +2 to Max HP")
    #     self.xp_n = self.calcNextLevel()

    #     while True:
    #         action = input()
    #         action = action.lower

    #         if action == "1" or action == "attack":
    #             #increase attack by 2
    #             print ("Are you sure you want to spend it on Attack? (y/n)")
    #             yn = input('>>> ')
    #             yn = yn.lower()

    #             if yn == "y" or yn == "yes":
    #                 self.attack += 2
    #                 print ("Awesome, your attack increased by 2 and is now {}".format(self.attack))
    #                 break
    #             elif yn == "n" or yn == "no":
    #                 print ("Ok, what would you like to spend it on?")

    #         elif action == "2" or action == "defence":
    #             #increase defence by 2
    #             print ("Are you sure you want to spend it on Defence? (y/n)")
    #             yn = input('>>> ')
    #             yn = yn.lower()

    #             if yn == "y" or yn == "yes":
    #                 self.defence += 2
    #                 print ("Awesome, your Defence increased by 2 and is now {}".format(self.defence))
    #                 break
    #             elif yn == "n" or yn == "no":
    #                 print ("Ok, what would you like to spend it on?")

    #         elif action == "3" or action == "agility":
    #             #increase agility by 10
    #             print ("Are you sure you want to spend it on Agility? (y/n)")
    #             yn = input('>>> ')
    #             yn = yn.lower()

    #             if yn == "y" or yn == "yes":
    #                 self.agility += 2
    #                 print ("Awesome, your agility increased by 2 and is now {}".format(self.agility))
    #                 break
    #             elif yn == "n" or yn == "no":
    #                 print ("Ok, what would you like to spend it on?")

    #         elif action == "4" or action == "max hp" or action == "hp" or action == "maxhp":
    #             #increase hp by 5
    #             print ("Are you sure you want to spend it on Max HP? (y/n)")
    #             yn = input('>>> ')
    #             yn = yn.lower()

    #             if yn == "y" or yn == "yes":
    #                 self.hp_m += 2
    #                 self.hp_c += 2
    #                 print ("Awesome, your HP increased by 2 and is now {}/{}".format(self.hp_c,self.hp_m))
    #                 break
    #             elif yn == "n" or yn == "no":
    #                 print ("Ok, what would you like to spend it on?")

    #         else:
    #             print ("Either spell it correctly or use the corresponding number")

    #     print("You are now level: {}".format(self.level))
    #     print("You will level up again when you have {} XP".format(self.xp_n))

    def pickleSaveChar(self, fileName):
        pickleFile = open(fileName, 'wb')
        pickle.dump(self, pickleFile)
        pickleFile.close()
            
    def pickleLoadChar(self, fileName):
        pickleFile = open(fileName, 'rb')
        char = pickle.load(pickleFile)
        pickleFile.close()
        return char

    def movement(self, direction):
        if direction == "north":
            self.cords[0] -= 1
        elif direction == "south":
            self.cords[0] += 1
        elif direction == "west":
            self.cords[1] -= 1
        elif direction == "east":
            self.cords[1] += 1

    def level_up(self, stat):
        self.xp_c = self.xp_c - self.xp_n
        self.xp_n = self.calcNextLevel()
        if stat == "attack":
            self.attack += 2

        elif stat == "defence":
            self.defence += 2

        elif stat == "agility":
            self.agility += 2

        elif stat == "hp":
            self.hp_m += 2
            self.hp_c += 2
    
            
# my_char = Character("Bill", 1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
# my_char.print_stats()
# my_char.pickleSaveChar("test.data")
# my_char2 = Character.pickleLoadChar(None, "test.data")
# my_char2.print_stats()
